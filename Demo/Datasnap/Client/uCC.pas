//
// Created by the DataSnap proxy generator.
// 23/04/2017 10:24:52
//

unit uCC;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.DBXJSONReflect;

type
  TORMBrClient = class(TDSAdminRestClient)
  private
    FmasterCommand: TDSRestCommand;
    FmasterCommand_Cache: TDSRestCommand;
    FnextpacketCommand: TDSRestCommand;
    FnextpacketCommand_Cache: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function master(const ARequestFilter: string = ''): TJSONArray;
    function master_Cache(const ARequestFilter: string = ''): IDSRestCachedJSONArray;
    function nextpacket(const ARequestFilter: string = ''): TJSONArray;
    function nextpacket_Cache(const ARequestFilter: string = ''): IDSRestCachedJSONArray;
  end;

const
  TORMBr_master: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONArray')
  );

  TORMBr_master_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TORMBr_nextpacket: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONArray')
  );

  TORMBr_nextpacket_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

implementation

function TORMBrClient.master(const ARequestFilter: string): TJSONArray;
begin
  if FmasterCommand = nil then
  begin
    FmasterCommand := FConnection.CreateCommand;
    FmasterCommand.RequestType := 'GET';
    FmasterCommand.Text := 'TORMBr.master';
    FmasterCommand.Prepare(TORMBr_master);
  end;
  FmasterCommand.Execute(ARequestFilter);
  Result := TJSONArray(FmasterCommand.Parameters[0].Value.GetJSONValue(FInstanceOwner));
end;

function TORMBrClient.master_Cache(const ARequestFilter: string): IDSRestCachedJSONArray;
begin
  if FmasterCommand_Cache = nil then
  begin
    FmasterCommand_Cache := FConnection.CreateCommand;
    FmasterCommand_Cache.RequestType := 'GET';
    FmasterCommand_Cache.Text := 'TORMBr.master';
    FmasterCommand_Cache.Prepare(TORMBr_master_Cache);
  end;
  FmasterCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONArray.Create(FmasterCommand_Cache.Parameters[0].Value.GetString);
end;

function TORMBrClient.nextpacket(const ARequestFilter: string): TJSONArray;
begin
  if FnextpacketCommand = nil then
  begin
    FnextpacketCommand := FConnection.CreateCommand;
    FnextpacketCommand.RequestType := 'GET';
    FnextpacketCommand.Text := 'TORMBr.nextpacket';
    FnextpacketCommand.Prepare(TORMBr_nextpacket);
  end;
  FnextpacketCommand.Execute(ARequestFilter);
  Result := TJSONArray(FnextpacketCommand.Parameters[0].Value.GetJSONValue(FInstanceOwner));
end;

function TORMBrClient.nextpacket_Cache(const ARequestFilter: string): IDSRestCachedJSONArray;
begin
  if FnextpacketCommand_Cache = nil then
  begin
    FnextpacketCommand_Cache := FConnection.CreateCommand;
    FnextpacketCommand_Cache.RequestType := 'GET';
    FnextpacketCommand_Cache.Text := 'TORMBr.nextpacket';
    FnextpacketCommand_Cache.Prepare(TORMBr_nextpacket_Cache);
  end;
  FnextpacketCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONArray.Create(FnextpacketCommand_Cache.Parameters[0].Value.GetString);
end;

constructor TORMBrClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TORMBrClient.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TORMBrClient.Destroy;
begin
  FmasterCommand.DisposeOf;
  FmasterCommand_Cache.DisposeOf;
  FnextpacketCommand.DisposeOf;
  FnextpacketCommand_Cache.DisposeOf;
  inherited;
end;

end.

