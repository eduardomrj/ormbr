unit uCM;

interface

uses
  System.SysUtils, System.Classes, uCC, Datasnap.DSClientRest;

type
  TClientModule1 = class(TDataModule)
    DSRestConnection1: TDSRestConnection;
  private
    FInstanceOwner: Boolean;
    FORMBrClient: TORMBrClient;
    function GetORMBrClient: TORMBrClient;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
    property ORMBrClient: TORMBrClient read GetORMBrClient write FORMBrClient;
  end;

var
  ClientModule1: TClientModule1;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

constructor TClientModule1.Create(AOwner: TComponent);
begin
  inherited;
  FInstanceOwner := True;
end;

destructor TClientModule1.Destroy;
begin
  FORMBrClient.Free;
  inherited;
end;

function TClientModule1.GetORMBrClient: TORMBrClient;
begin
  if FORMBrClient = nil then
    FORMBrClient:= TORMBrClient.Create(DSRestConnection1, FInstanceOwner);
  Result := FORMBrClient;
end;

end.
