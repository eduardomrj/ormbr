unit uSM;

interface

uses
  System.SysUtils, System.Classes, System.Json,
  DataSnap.DSProviderDataModuleAdapter,
  Datasnap.DSServer,
  Datasnap.DSAuth,
  Datasnap.DSSession,
  System.Generics.Collections,
  /// ORMBr JSON e DataSnap
  ormbr.rest.json,
  ormbr.jsonutils.datasnap,
  /// ORMBr Conex�o database
  ormbr.factory.firedac,
  ormbr.factory.interfaces,
  ormbr.types.database,
  /// ORMBr
  ormbr.container.objectset,
  ormbr.container.objectset.interfaces,
  ormbr.session.manager,
  ormbr.model.master,
  ormbr.dml.generator.sqlite,
  uSC,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client;

type
  TORMBr = class(TDSServerModule)
    FDConnection1: TFDConnection;
  private
    { Private declarations }
    FSession: TDSSession;
    FConnectionKey: string;
    FMasterKey: string;
    FConnection: IDBConnection;
    FMaster: IContainerObjectSet<Tmaster>;
    procedure DeleteKeys;
    procedure AddKeys;
    procedure GeneratorKeys;
    procedure RecoversKeys;
  public
    { Public declarations }
    function master: TJSONArray;
    function nextpacket: TJSONArray;
  end;

implementation

{$R *.dfm}

{ TServerMethods1 }

procedure TORMBr.AddKeys;
begin
  GeneratorKeys;
  TServerContainer1.GetDictionary.Add(FConnectionKey, TFactoryFireDAC.Create(FDConnection1, dnSQLite));
  FConnection := TServerContainer1.GetDictionary.Items[FConnectionKey] as TFactoryFireDAC;
  TServerContainer1.GetDictionary.Add(FMasterKey, TContainerObjectSet<Tmaster>.Create(FConnection, 10));
end;

procedure TORMBr.DeleteKeys;
begin
  GeneratorKeys;
  if TServerContainer1.GetDictionary.ContainsKey(FConnectionKey) then
    TServerContainer1.GetDictionary.Remove(FConnectionKey);
  if TServerContainer1.GetDictionary.ContainsKey(FMasterKey) then
    TServerContainer1.GetDictionary.Remove(FMasterKey);
end;

procedure TORMBr.GeneratorKeys;
begin
  FSession := TDSSessionManager.GetThreadSession;
  FConnectionKey := 'Connection_' + IntToStr(FSession.Id);
  FMasterKey := 'Master_' + IntToStr(FSession.Id);
end;

procedure TORMBr.RecoversKeys;
begin
  GeneratorKeys;
  FConnection := TServerContainer1.GetDictionary.Items[FConnectionKey] as TFactoryFireDAC;
  FMaster := TServerContainer1.GetDictionary.Items[FMasterKey] as TContainerObjectSet<Tmaster>;
end;

function TORMBr.master: TJSONArray;
var
  LMasterList: TObjectList<Tmaster>;
begin
  DeleteKeys;
  AddKeys;
  RecoversKeys;
  LMasterList := TObjectList<Tmaster>.Create;
  try
    LMasterList := FMaster.Find;
    Result := TORMBrJSONUtil.JSONStringToJSONArray<Tmaster>(LMasterList);
  finally
    LMasterList.Free;
  end;
end;

function TORMBr.nextpacket: TJSONArray;
var
  LMasterList: TObjectList<Tmaster>;
begin
  RecoversKeys;
  LMasterList := TObjectList<Tmaster>.Create;
  try
    FMaster.NextPacket(LMasterList);
    Result := TORMBrJSONUtil.JSONStringToJSONArray<Tmaster>(LMasterList);
  finally
    LMasterList.Free;
  end;
end;

end.

