{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.objects.manager;

interface

uses
  DB,
  Rtti,
  Types,
  Classes,
  SysUtils,
  Variants,
  Generics.Collections,
  /// ormbr
  ormbr.criteria,
  ormbr.types.mapping,
  ormbr.mapping.classes,
  ormbr.command.factory,
  ormbr.factory.interfaces,
  ormbr.mapping.explorer,
  ormbr.mapping.explorerstrategy;

type
  TObjectManager<M: class, constructor> = class abstract
  private
    FOwner: TObject;
    FUpdateInternal: M;
    FObjectInternal: M;
    FFetchingRecords: Boolean;
    FModifiedFields: TList<string>;
    procedure FillAssociation(AObject: M);
    procedure ExecuteOneToOne(AObject: TObject; AProperty: TRttiProperty;
      AAssociation: TAssociationMapping);
    procedure ExecuteOneToMany(AObject: TObject; AProperty: TRttiProperty;
      AAssociation: TAssociationMapping);
    procedure ModifyFieldsCompare(AObjectSource, AObjectUpdate: M);
  protected
    FConnection: IDBConnection;
    /// <summary>
    /// F�brica de comandos a serem executados
    /// </summary>
    FDMLCommandFactory: TDMLCommandFactoryAbstract;
    /// <summary>
    /// Instancia a class que mapea todas as class do tipo Entity
    /// </summary>
    FExplorer: IMappingExplorerStrategy;
    /// <summary>
    /// Controle de pagina��o vindo do banco de dados
    /// </summary>
    FPageSize: Integer;
  public
    constructor Create(const AOwner: TObject; const AConnection: IDBConnection; const APageSize: Integer); virtual;
    destructor Destroy; override;
    procedure InsertInternal(AObject: M); virtual;
    procedure UpdateInternal(AObject: M); virtual;
    procedure DeleteInternal(AObject: M); virtual;
    procedure ModifyInternal(AObjectSource: M); virtual;
    function SelectInternalAll: IDBResultSet; virtual;
    function SelectInternalID(AID: TValue): IDBResultSet; virtual;
    function SelectInternal(ASQL: String): IDBResultSet; virtual;
    function SelectInternalWhere(AWhere: string; AOrderBy: string): string; virtual;
    function GetDMLCommand: string;
    function Find: TObjectList<M>; overload;
    function Find(ASQL: String): TObjectList<M>; overload;
    function Find(AID: TValue): M; overload;
    function FindWhere(AWhere: string; AOrderBy: string): TObjectList<M>;
    function ExistSequence: Boolean;
    function NextPacket: IDBResultSet; virtual;
    procedure NextPacketList(AObjectList: TObjectList<M>); virtual;
    property Explorer: IMappingExplorerStrategy read FExplorer;
    property FetchingRecords: Boolean read FFetchingRecords write FFetchingRecords;
    property ModifiedFields: TList<string> read FModifiedFields write FModifiedFields;
  end;

implementation

uses
  TypInfo,
  ormbr.objectset.bind,
  ormbr.types.database,
  ormbr.objects.helper,
  ormbr.mapping.attributes,
  ormbr.mapping.rttiutils,
  ormbr.session.manager,
  ormbr.rtti.helper,
  ormbr.types.blob;

{ TObjectManager<M> }

constructor TObjectManager<M>.Create(const AOwner: TObject; const AConnection: IDBConnection;
  const APageSize: Integer);
begin
  FOwner := AOwner;
  FPageSize := APageSize;
  if not (AOwner is TSessionAbstract<M>) then
    raise Exception.Create('O Object Manager n�o deve ser inst�nciada diretamente, use as classes TSessionObject<M> ou TSessionDataSet<M>');

  FConnection := AConnection;
  FExplorer := TMappingExplorer.GetInstance;
  FObjectInternal := M.Create;
  /// <summary>
  /// Lista de campos alterados
  /// </summary>
  FModifiedFields := TList<string>.Create;
  /// <summary>
  /// Fabrica de comandos SQL
  /// </summary>
  FDMLCommandFactory := TDMLCommandFactory.Create(FObjectInternal,
                                                  AConnection,
                                                  AConnection.GetDriverName);
end;

procedure TObjectManager<M>.ModifyFieldsCompare(AObjectSource, AObjectUpdate: M);
const
  cPropertyTypes = [tkUnknown,tkInterface,tkClass,tkClassRef,tkPointer,tkProcedure];
var
  LRttiType: TRttiType;
  LProperty: TRttiProperty;
  LColumn: TCustomAttribute;
  LSourceAsPointer, oUpdateAsPointer: Pointer;
begin
  LRttiType := TRttiSingleton.GetInstance.GetRttiType(AObjectSource.ClassType);
  try
    Move(AObjectSource, LSourceAsPointer, SizeOf(Pointer));
    Move(AObjectUpdate, oUpdateAsPointer, SizeOf(Pointer));
    /// <summary>
    /// Loop na lista de propriedades
    /// </summary>
    for LProperty in LRttiType.GetProperties do
    begin
      /// <summary>
      /// Checa se no modelo o campo deve fazer parte do UPDATE
      /// </summary>
      if LProperty.IsNoUpdate then
        Continue;
      /// <summary>
      /// Valida��o para entrar no IF somente propriedades que o tipo nao esteja na lista
      /// </summary>
      if not (LProperty.PropertyType.TypeKind in cPropertyTypes) then
      begin
        /// <summary>
        /// Se o tipo da property for tkRecord provavelmente tem Nullable nela
        /// Se n�o for tkRecord entra no ELSE e pega o valor de forma direta
        /// </summary>
        if LProperty.PropertyType.TypeKind = tkRecord then // Nullable, Proxy ou TBlob
        begin
          if TRttiSingleton.GetInstance.IsBlob(LProperty.PropertyType.Handle) then
          begin
            if LProperty.GetNullableValue(LSourceAsPointer).AsType<TBlob>.ToSize <>
               LProperty.GetNullableValue(oUpdateAsPointer).AsType<TBlob>.ToSize then
            begin
              LColumn := LProperty.GetColumn;
              if LColumn <> nil then
                FModifiedFields.Add(Column(LColumn).ColumnName);
            end;
          end
          else
          if TRttiSingleton.GetInstance.IsNullable(LProperty.PropertyType.Handle) then
          begin
            if LProperty.GetNullableValue(LSourceAsPointer).AsVariant <>
               LProperty.GetNullableValue(oUpdateAsPointer).AsVariant then
            begin
              LColumn := LProperty.GetColumn;
              if LColumn <> nil then
                FModifiedFields.Add(Column(LColumn).ColumnName);
            end;
          end;
        end
        else
        begin
          if LProperty.GetNullableValue(LSourceAsPointer).AsVariant <>
             LProperty.GetNullableValue(oUpdateAsPointer).AsVariant then
          begin
            LColumn := LProperty.GetColumn;
            if LColumn <> nil then
              FModifiedFields.Add(Column(LColumn).ColumnName);
          end;
        end;
      end;
    end;
  except
    raise;
  end;
end;

destructor TObjectManager<M>.Destroy;
begin
  FExplorer := nil;
  FDMLCommandFactory.Free;
  FObjectInternal.Free;
  FModifiedFields.Free;
  if Assigned(FUpdateInternal) then
    FUpdateInternal.Free;
  inherited;
end;

procedure TObjectManager<M>.DeleteInternal(AObject: M);
begin
  FDMLCommandFactory.GeneratorDelete(AObject);
end;

function TObjectManager<M>.SelectInternalAll: IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorSelectAll(M, FPageSize);
end;

function TObjectManager<M>.SelectInternalID(AID: TValue): IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorSelectID(M, AID);
end;

function TObjectManager<M>.SelectInternalWhere(AWhere: string; AOrderBy: string): string;
begin
  Result := FDMLCommandFactory.GeneratorSelectWhere(M, AWhere, AOrderBy, FPageSize);
end;

procedure TObjectManager<M>.FillAssociation(AObject: M);
var
  LAssociationList: TAssociationMappingList;
  LAssociation: TAssociationMapping;
begin
  LAssociationList := FExplorer.GetMappingAssociation(AObject.ClassType);
  if LAssociationList <> nil then
  begin
    for LAssociation in LAssociationList do
    begin
       if LAssociation.Multiplicity in [OneToOne, ManyToOne] then
          ExecuteOneToOne(AObject, LAssociation.PropertyRtti, LAssociation)
       else
       if LAssociation.Multiplicity in [OneToMany, ManyToMany] then
          ExecuteOneToMany(AObject, LAssociation.PropertyRtti, LAssociation);
    end;
  end;
end;

procedure TObjectManager<M>.ExecuteOneToOne(AObject: TObject; AProperty: TRttiProperty;
  AAssociation: TAssociationMapping);
var
 LResultSet: IDBResultSet;
begin
  LResultSet := FDMLCommandFactory.GeneratorSelectOneToOne(AObject,
                                                           AProperty.PropertyType.AsInstance.MetaclassType,
                                                           AAssociation);
  try
    while LResultSet.NotEof do
    begin
      TBindObject.GetInstance.SetFieldToProperty(LResultSet,
                                                 AProperty.GetNullableValue(AObject).AsObject,
                                                 AAssociation);
    end;
  finally
    LResultSet.Close;
  end;
end;

procedure TObjectManager<M>.ExecuteOneToMany(AObject: TObject; AProperty: TRttiProperty;
  AAssociation: TAssociationMapping);
var
  LPropertyType: TRttiType;
  LPropertyObject: TObject;
  LResultSet: IDBResultSet;
begin
  LPropertyType := AProperty.PropertyType;
  LPropertyType := TRttiSingleton.GetInstance.GetListType(LPropertyType);
  LResultSet := FDMLCommandFactory.GeneratorSelectOneToMany(AObject,
                                                            LPropertyType.AsInstance.MetaclassType,
                                                            AAssociation);
  try
    while LResultSet.NotEof do
    begin
      /// <summary>
      /// Instancia o objeto da lista
      /// </summary>
      LPropertyObject := LPropertyType.AsInstance.MetaclassType.Create;
      /// <summary>
      /// Preenche o objeto com os dados do ResultSet
      /// </summary>
      TBindObject.GetInstance.SetFieldToProperty(LResultSet, LPropertyObject, AAssociation);
      /// <summary>
      /// Adiciona o objeto a lista
      /// </summary>
      TRttiSingleton.GetInstance.MethodCall(AProperty.GetNullableValue(AObject).AsObject,'Add',[LPropertyObject]);
    end;
  finally
    LResultSet.Close;
  end;
end;

function TObjectManager<M>.ExistSequence: Boolean;
begin
  Result := FDMLCommandFactory.ExistSequence;
end;

function TObjectManager<M>.GetDMLCommand: string;
begin
  Result := FDMLCommandFactory.GetDMLCommand;
end;

function TObjectManager<M>.NextPacket: IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorNextPacket;
  if Result.FetchingAll then
    FFetchingRecords := True;
end;

procedure TObjectManager<M>.NextPacketList(AObjectList: TObjectList<M>);
var
 LResultSet: IDBResultSet;
begin
  LResultSet := NextPacket;
  try
    while LResultSet.NotEof do
    begin
      AObjectList.Add(M.Create);
      TBindObject.GetInstance.SetFieldToProperty(LResultSet, TObject(AObjectList.Last));
      /// <summary>
      /// Alimenta registros das associa��es existentes 1:1 ou 1:N
      /// </summary>
      FillAssociation(AObjectList.Last);
    end;
  finally
    /// <summary>
    /// Fecha o DataSet interno para limpar os dados dele da mem�ria.
    /// </summary>
    LResultSet.Close;
  end;
end;

function TObjectManager<M>.SelectInternal(ASQL: String): IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorSelect(ASQL, FPageSize);
end;

procedure TObjectManager<M>.UpdateInternal(AObject: M);
begin
  /// <summary>
  /// Gera a lista com as propriedades que foram alteradas
  /// Se Count > 0, a lista est� sendo preenchida na classe.
  /// </summary>
  if FModifiedFields.Count = 0 then
    ModifyFieldsCompare(AObject, FUpdateInternal);
  /// <summary>
  ///
  /// </summary>
  FDMLCommandFactory.GeneratorUpdate(TObject(AObject), FModifiedFields);
end;

procedure TObjectManager<M>.InsertInternal(AObject: M);
begin
  FDMLCommandFactory.GeneratorInsert(AObject);
end;

procedure TObjectManager<M>.ModifyInternal(AObjectSource: M);
const
  cPropertyTypes = [tkUnknown,tkInterface,tkClass,tkClassRef,tkPointer,tkProcedure];
var
  LRttiType: TRttiType;
  LProperty: TRttiProperty;
  LSourceAsPointer, oUpdateAsPointer: Pointer;
begin
  /// <summary>
  /// Toda vez libera FUpdateInternal da mem�ria, para que n�o fique vestigio de
  /// informa��es antigos de outros UPDATEs na vari�vel.
  /// </summary>
  if Assigned(FUpdateInternal) then
    FUpdateInternal.Free;
  /// <summary>
  /// Como toda vez que chamado esse m�todo o FUpdateInternal � liberado acima
  /// ele tem que ser recriado.
  /// </summary>
  FUpdateInternal := M.Create;
  LRttiType := TRttiSingleton.GetInstance.GetRttiType(AObjectSource.ClassType);
  try
    Move(AObjectSource, LSourceAsPointer, SizeOf(Pointer));
    Move(FUpdateInternal, oUpdateAsPointer, SizeOf(Pointer));
    /// <summary>
    /// Loop na lista de propriedades
    /// </summary>
    for LProperty in LRttiType.GetProperties do
    begin
      /// <summary>
      /// Checa se no modelo o campo deve fazer parte do UPDATE
      /// </summary>
      if LProperty.IsNoUpdate then
        Continue;
      /// <summary>
      /// Valida��o para entrar no IF somente propriedades que o tipo n�o esteja na lista
      /// </summary>
      if not (LProperty.PropertyType.TypeKind in cPropertyTypes) then
        LProperty.SetValue(oUpdateAsPointer, LProperty.GetNullableValue(LSourceAsPointer));
    end;
  except
    raise;
  end;
end;

function TObjectManager<M>.Find(ASQL: String): TObjectList<M>;
var
 LResultSet: IDBResultSet;
begin
  Result := TObjectList<M>.Create;
  if ASQL = '' then
    LResultSet := SelectInternalAll
  else
    LResultSet := SelectInternal(ASQL);
  try
    while LResultSet.NotEof do
    begin
      TBindObject.GetInstance.SetFieldToProperty(LResultSet, TObject(Result.Items[Result.Add(M.Create)]));
      /// <summary>
      /// Alimenta registros das associa��es existentes 1:1 ou 1:N
      /// </summary>
      FillAssociation(Result.Items[Result.Count -1]);
    end;
  finally
    LResultSet.Close;
  end;
end;

function TObjectManager<M>.Find: TObjectList<M>;
begin
  Result := Find('');
end;

function TObjectManager<M>.Find(AID: TValue): M;
var
 LResultSet: IDBResultSet;
begin
  LResultSet := SelectInternalID(AID);
  try
    if LResultSet.RecordCount = 1 then
    begin
      Result := M.Create;
      TBindObject.GetInstance.SetFieldToProperty(LResultSet, TObject(Result));
      /// <summary>
      /// Alimenta registros das associa��es existentes 1:1 ou 1:N
      /// </summary>
      FillAssociation(Result);
    end
    else
      Result := nil;
  finally
    /// <summary>
    /// Fecha o DataSet interno para limpar os dados dele da mem�ria.
    /// </summary>
    LResultSet.Close;
  end;
end;

function TObjectManager<M>.FindWhere(AWhere: string; AOrderBy: string): TObjectList<M>;
begin
  Result := Find(SelectInternalWhere(AWhere, AOrderBy));
end;

end.

