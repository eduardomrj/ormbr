{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.rtti.helper;

interface

uses
  Rtti,
  SysUtils,
  TypInfo,
  ormbr.mapping.rttiutils,
  ormbr.types.nullable,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  ormbr.mapping.classes;

type
  TRttiTypeHelper = class helper for TRttiType
  public
    function GetPrimaryKey: TArray<TCustomAttribute>;
    function GetAggregateField: TArray<TCustomAttribute>;
  end;

  TRttiPropertyHelper = class helper for TRttiProperty
  public
    function  IsNoUpdate: Boolean;
    function  IsNoInsert: Boolean;
    function  IsNotNull: Boolean;
    function  IsJoinColumn: Boolean;
    function  IsCheck: Boolean;
    function  IsUnique: Boolean;
    function  IsHidden: Boolean;
    function  IsRequired: Boolean;
    function  IsPrimaryKey(AClass: TClass): Boolean;
    function  GetAssociation: TCustomAttribute;
    function  GetRestriction: TCustomAttribute;
    function  GetDictionary: TCustomAttribute;
    function  GetCalcField: TCustomAttribute;
    function  GetColumn: TCustomAttribute;
    function  GetNotNullConstraint: TCustomAttribute;
    function  GetZeroConstraint: TCustomAttribute;
    function  GetNullableValue(AInstance: Pointer): TValue;
    function  GetIndex: Integer;
    procedure SetNullableValue(AInstance: Pointer; ATypeInfo: PTypeInfo; AValue: Variant);
  end;

implementation

uses
  ormbr.mapping.explorer;

{ TRttiPropertyHelper }

function TRttiPropertyHelper.GetAssociation: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is Association then // Association
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetCalcField: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is CalcField then // CalcField
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetColumn: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is Column then // Column
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetDictionary: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is Dictionary then // Dictionary
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetIndex: Integer;
begin
  Result := (Self as TRttiInstanceProperty).Index +1;
end;

function TRttiPropertyHelper.GetNotNullConstraint: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is NotNullConstraint then // NotNullConstraint
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetNullableValue(AInstance: Pointer): TValue;
var
  LValue: TValue;
  LValueField: TRttiField;
  LHasValueField: TRttiField;
begin
  if TRttiSingleton.GetInstance.IsNullable(Self.PropertyType.Handle) then
  begin
    LValue := Self.GetValue(AInstance);
    LHasValueField := Self.PropertyType.GetField('FHasValue');
    if Assigned(LHasValueField) then
    begin
      LValueField := Self.PropertyType.GetField('FValue');
      if Assigned(LValueField) then
         Result := LValueField.GetValue(LValue.GetReferenceToRawData);
    end
  end
  else
     Result := Self.GetValue(AInstance);
end;

function TRttiPropertyHelper.GetRestriction: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is Restrictions then // Restrictions
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetZeroConstraint: TCustomAttribute;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is ZeroConstraint then // ZeroConstraint
         Exit(LAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.IsCheck: Boolean;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is Check then // Check
         Exit(True);
   end;
   Exit(False);
end;

function TRttiPropertyHelper.IsHidden: Boolean;
var
  LAttribute: TCustomAttribute;
begin
   LAttribute := Self.GetRestriction;
   if LAttribute <> nil then
   begin
     if Hidden in Restrictions(LAttribute).Restrictions then
       Exit(True);
   end;
   Exit(False);
end;

function TRttiPropertyHelper.IsNoInsert: Boolean;
var
  LAttribute: TCustomAttribute;
begin
   LAttribute := Self.GetRestriction;
   if LAttribute <> nil then
   begin
     if NoInsert in Restrictions(LAttribute).Restrictions then
       Exit(True);
   end;
   Exit(False);
end;

function TRttiPropertyHelper.IsJoinColumn: Boolean;
var
  LAttribute: TCustomAttribute;
begin
   for LAttribute in Self.GetAttributes do
   begin
      if LAttribute is JoinColumn then // JoinColumn
         Exit(True);
   end;
   Exit(False);
end;

function TRttiPropertyHelper.IsNotNull: Boolean;
var
  LAttribute: TCustomAttribute;
begin
   LAttribute := Self.GetRestriction;
   if LAttribute <> nil then
   begin
     if NotNull in Restrictions(LAttribute).Restrictions then
       Exit(True);
   end;
   Exit(False);
end;

function TRttiPropertyHelper.IsNoUpdate: Boolean;
var
  LAttribute: TCustomAttribute;
begin
  LAttribute := Self.GetRestriction;
  if LAttribute <> nil then
  begin
    if NoUpdate in Restrictions(LAttribute).Restrictions then
      Exit(True);
  end;
  Exit(False);
end;

function TRttiPropertyHelper.IsPrimaryKey(AClass: TClass): Boolean;
var
  LPrimaryKey: TPrimaryKeyMapping;
  LColumnName: string;
begin
  LPrimaryKey := TMappingExplorer.GetInstance.GetMappingPrimaryKey(AClass);
  if LPrimaryKey <> nil then
  begin
    for LColumnName in LPrimaryKey.Columns do
      if SameText(LColumnName, Self.Name) then
        Exit(True);
  end;
  Exit(False);
end;

function TRttiPropertyHelper.IsRequired: Boolean;
var
  LAttribute: TCustomAttribute;
begin
  LAttribute := Self.GetRestriction;
  if LAttribute <> nil then
  begin
    if Required in Restrictions(LAttribute).Restrictions then
      Exit(True);
  end;
  Exit(False);
end;

function TRttiPropertyHelper.IsUnique: Boolean;
var
  LAttribute: TCustomAttribute;
begin
   LAttribute := Self.GetRestriction;
   if LAttribute <> nil then
   begin
     if Unique in Restrictions(LAttribute).Restrictions then
       Exit(True);
   end;
   Exit(False);
end;

procedure TRttiPropertyHelper.SetNullableValue(AInstance: Pointer;
  ATypeInfo: PTypeInfo; AValue: Variant);
begin
   if ATypeInfo = TypeInfo(Nullable<Integer>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Integer>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<String>) then
      Self.SetValue(AInstance, TValue.From(Nullable<String>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<TDateTime>) then
      Self.SetValue(AInstance, TValue.From(Nullable<TDateTime>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Currency>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Currency>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Double>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Double>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Boolean>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Boolean>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<TDate>) then
      Self.SetValue(AInstance, TValue.From(Nullable<TDate>.Create(AValue)))
end;

{ TRttiTypeHelper }

//function TRttiTypeHelper.GetAssociation: TArray<TCustomAttribute>;
//var
//  oProperty: TRttiProperty;
//  LAttrib: TCustomAttribute;
//  LLength: Integer;
//begin
//   LLength := -1;
//   for oProperty in Self.GetProperties do
//   begin
//      for LAttrib in oProperty.GetAttributes do
//      begin
//         if LAttrib is Association then // Association
//         begin
//           Inc(LLength);
//           SetLength(Result, LLength+1);
//           Result[LLength] := LAttrib;
//         end;
//      end;
//   end;
//end;

function TRttiTypeHelper.GetAggregateField: TArray<TCustomAttribute>;
var
  LAttrib: TCustomAttribute;
  LLength: Integer;
begin
  LLength := -1;
  for LAttrib in Self.GetAttributes do
  begin
     if LAttrib is AggregateField then // AggregateField
     begin
       Inc(LLength);
       SetLength(Result, LLength+1);
       Result[LLength] := LAttrib;
     end;
  end;
end;

function TRttiTypeHelper.GetPrimaryKey: TArray<TCustomAttribute>;
var
  LAttrib: TCustomAttribute;
  LLength: Integer;
begin
  LLength := -1;
  for LAttrib in Self.GetAttributes do
  begin
     if LAttrib is PrimaryKey then // PrimaryKey
     begin
       Inc(LLength);
       SetLength(Result, LLength+1);
       Result[LLength] := LAttrib;
     end;
  end;
end;

end.
