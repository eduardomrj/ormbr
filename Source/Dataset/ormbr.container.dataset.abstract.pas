{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.container.dataset.abstract;

interface

uses
  DB,
  RTTi,
  Classes,
  SysUtils,
  Generics.Collections,
  /// ormbr
  ormbr.container.dataset.interfaces,
  ormbr.factory.interfaces,
  ormbr.dataset.adapter;

type
  TContainerDataSetAbstract<M: class, constructor> = class abstract(TInterfacedObject, IContainerDataSet<M>)
  private
    function GetDataSetBase: TDataSetAdapter<M>;
    function GetAutoNextPacket: Boolean;
    procedure SetAutoNextPacket(const Value: Boolean);
    function GetCurrentRecordInternal: M;
  protected
    FDataSetBase: TDataSetAdapter<M>;
    FConnection: IDBConnection;
  public
    procedure Lazy(AOwner: M); virtual; abstract;
    procedure Open(AID: TValue); overload; virtual; abstract;
    procedure Open; overload; virtual; abstract;
    procedure Open(AWhere: string; AOrderBy: string = ''); overload; virtual; abstract;
    procedure OpenWhere(AWhere: string); virtual; abstract;
    procedure Insert; virtual; abstract;
    procedure Append; virtual; abstract;
    procedure Post; virtual; abstract;
    procedure Edit; virtual; abstract;
    procedure Delete; virtual; abstract;
    procedure Close; virtual; abstract;
    procedure Cancel; virtual; abstract;
    procedure CancelUpdates; virtual; abstract;
    procedure Save(AObject: M); virtual; abstract;
    procedure ApplyUpdates(MaxErros: Integer); virtual; abstract;
    procedure AddCalcField(AFieldName: string;
                           AFieldType: TFieldType;
                           ASize: Integer = 0;
                           AAlignment: TAlignment = taLeftJustify;
                           ADisplayFormat: string = ''); virtual; abstract;
    procedure AddLookupField(AFieldName: string;
                             AKeyFields: string;
                             ALookupDataSet: TObject;
                             ALookupKeyFields: string;
                             ALookupResultField: string); virtual; abstract;
    procedure AddAggregateField(AFieldName: string;
                                AExpression: string;
                                AAlignment: TAlignment = taLeftJustify;
                                ADisplayFormat: string = ''); virtual; abstract;
    procedure NextPacket; virtual; abstract;
    /// ObjectSet
    function Find: TObjectList<M>; overload; virtual; abstract;
    function Find(AID: TValue): M; overload; virtual; abstract;
    function Find(AWhere: string; AOrderBy: string = ''): TObjectList<M>; overload; virtual; abstract;
    function GetDataSetInternal: TDataSet; virtual; abstract;
    property DataSet: TDataSetAdapter<M> read GetDataSetBase;
    property Current: M read GetCurrentRecordInternal;
    property AutoNextPacket: Boolean read GetAutoNextPacket write SetAutoNextPacket;
  end;

implementation

{ TContainerDataSetAbstract<M> }

function TContainerDataSetAbstract<M>.GetAutoNextPacket: Boolean;
begin
  Result := FDataSetBase.AutoNextPacket
end;

function TContainerDataSetAbstract<M>.GetCurrentRecordInternal: M;
begin
  Result := FDataSetBase.Current;
end;

function TContainerDataSetAbstract<M>.GetDataSetBase: TDataSetAdapter<M>;
begin
  Result := FDataSetBase;
end;

procedure TContainerDataSetAbstract<M>.SetAutoNextPacket(const Value: Boolean);
begin
  FDataSetBase.AutoNextPacket := Value;
end;

end.
