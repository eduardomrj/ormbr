{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.container.dataset.interfaces;

interface

uses
  DB,
  RTTi,
  Classes,
  Generics.Collections,
  ormbr.dataset.adapter;

type
  IContainerDataSet<M: class, constructor> = interface
    ['{67DC311E-06BF-4B41-93E1-FA66AB0D8537}']
  {$REGION 'Property Getters & Setters'}
    function GetDataSetBase: TDataSetAdapter<M>;
    function GetAutoNextPacket: Boolean;
    procedure SetAutoNextPacket(const Value: Boolean);
    function GetCurrentRecordInternal: M;
  {$ENDREGION}
    procedure Lazy(AOwner: M);
    procedure Open; overload;
    procedure Open(AID: TValue); overload;
    procedure Open(AWhere: string; AOrderBy: string); overload;
    procedure OpenWhere(AWhere: string);
    procedure Insert;
    procedure Append;
    procedure Post;
    procedure Edit;
    procedure Delete;
    procedure Close;
    procedure Cancel;
    procedure CancelUpdates;
    procedure Save(AObject: M);
    procedure ApplyUpdates(MaxErros: Integer);
    procedure AddCalcField(AFieldName: string;
                           AFieldType: TFieldType;
                           ASize: Integer = 0;
                           AAlignment: TAlignment = taLeftJustify;
                           ADisplayFormat: string = '');
    procedure AddLookupField(AFieldName: string;
                             AKeyFields: string;
                             ALookupDataSet: TObject;
                             ALookupKeyFields: string;
                             ALookupResultField: string);
    procedure AddAggregateField(AFieldName: string;
                                AExpression: string;
                                AAlignment: TAlignment = taLeftJustify;
                                ADisplayFormat: string = '');
    procedure NextPacket;
    /// ObjectSet
    function Find: TObjectList<M>; overload;
    function Find(AID: TValue): M; overload;
    function Find(AWhere: string; AOrderBy: string = ''): TObjectList<M>; overload;
    function GetDataSetInternal: TDataSet;
    property DataSet: TDataSetAdapter<M> read GetDataSetBase;
    property Current: M read GetCurrentRecordInternal;
    property AutoNextPacket: Boolean read GetAutoNextPacket write SetAutoNextPacket;
  end;

implementation

end.
