{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.container.dataset.strategy;

interface

uses
  DB,
  RTTi,
  Classes,
  SysUtils,
  Generics.Collections,
  /// ormbr
  ormbr.container.dataset.interfaces,
  ormbr.container.dataset.abstract,
  ormbr.factory.interfaces,
  ormbr.dataset.abstract;

type
  TContainerDataSetStrategy<M: class, constructor> = class(TContainerDataSetAbstract<M>)
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject); overload; virtual;
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer); overload; virtual;
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; AMasterObject: TObject); overload; virtual;
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet); overload; virtual;
    destructor Destroy; override;
    procedure Lazy(AOwner: M); override;
    procedure Open(AID: TValue); overload; override;
    procedure Open; overload; override;
    procedure Open(AWhere: string; AOrderBy: string = ''); overload; override;
    procedure OpenWhere(AWhere: string); override;
    procedure Insert; override;
    procedure Append; override;
    procedure Post; override;
    procedure Edit; override;
    procedure Delete; override;
    procedure Close; override;
    procedure Cancel; override;
    procedure CancelUpdates; override;
    procedure Save(AObject: M); override;
    procedure ApplyUpdates(MaxErros: Integer); override;
    procedure AddCalcField(AFieldName: string;
                           AFieldType: TFieldType;
                           ASize: Integer = 0;
                           AAlignment: TAlignment = taLeftJustify;
                           ADisplayFormat: string = ''); override;
    procedure AddLookupField(AFieldName: string;
                             AKeyFields: string;
                             ALookupDataSet: TObject;
                             ALookupKeyFields: string;
                             ALookupResultField: string); override;
    procedure AddAggregateField(AFieldName: string;
                                AExpression: string;
                                AAlignment: TAlignment = taLeftJustify;
                                ADisplayFormat: string = ''); override;
    procedure NextPacket; override;
    /// ObjectSet
    function Find: TObjectList<M>; overload; override;
    function Find(AID: TValue): M; overload; override;
    function Find(AWhere: string; AOrderBy: string = ''): TObjectList<M>; overload; override;
    function GetDataSetInternal: TDataSet; override;
  end;

implementation

{ TContainer }

constructor TContainerDataSetStrategy<M>.Create(AConnection: IDBConnection;
  ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject);
begin
  FConnection := AConnection;
end;

constructor TContainerDataSetStrategy<M>.Create(AConnection: IDBConnection;
  ADataSet: TDataSet; APageSize: Integer);
begin
  Create(AConnection, ADataSet, APageSize, nil);
end;

constructor TContainerDataSetStrategy<M>.Create(AConnection: IDBConnection; ADataSet: TDataSet);
begin
  Create(AConnection, ADataSet, -1, nil);
end;

procedure TContainerDataSetStrategy<M>.AddAggregateField(AFieldName,
  AExpression: string; AAlignment: TAlignment; ADisplayFormat: string);
begin
  inherited;
  FDataSetBase.AddAggregateField(AFieldName,
                                 AExpression,
                                 AAlignment,
                                 ADisplayFormat);
end;

procedure TContainerDataSetStrategy<M>.AddCalcField(AFieldName: string;
  AFieldType: TFieldType; ASize: Integer; AAlignment: TAlignment;
  ADisplayFormat: string);
begin
  inherited;
  FDataSetBase.AddCalcField(AFieldName,
                            AFieldType,
                            ASize,
                            AAlignment,
                            ADisplayFormat);
end;

procedure TContainerDataSetStrategy<M>.AddLookupField(AFieldName, AKeyFields: string;
  ALookupDataSet: TObject; ALookupKeyFields, ALookupResultField: string);
begin
  inherited;
  FDataSetBase.AddLookupField(AFieldName,
                              AKeyFields,
                              ALookupDataSet,
                              ALookupKeyFields,
                              ALookupResultField);
end;

procedure TContainerDataSetStrategy<M>.Append;
begin
  inherited;
  FDataSetBase.Append;
end;

procedure TContainerDataSetStrategy<M>.ApplyUpdates(MaxErros: Integer);
begin
  inherited;
  FDataSetBase.ApplyUpdates(MaxErros);
end;

procedure TContainerDataSetStrategy<M>.Cancel;
begin
  inherited;
  FDataSetBase.Cancel;
end;

procedure TContainerDataSetStrategy<M>.CancelUpdates;
begin
  inherited;
  FDataSetBase.CancelUpdates;
end;

procedure TContainerDataSetStrategy<M>.Close;
begin
  inherited;
  FDataSetBase.Close;
end;

constructor TContainerDataSetStrategy<M>.Create(AConnection: IDBConnection;
  ADataSet: TDataSet; AMasterObject: TObject);
begin
  Create(AConnection, ADataSet, -1, AMasterObject);
end;

procedure TContainerDataSetStrategy<M>.Delete;
begin
  inherited;
  FDataSetBase.Delete;
end;

destructor TContainerDataSetStrategy<M>.Destroy;
begin
  FDataSetBase.Free;
  inherited;
end;

procedure TContainerDataSetStrategy<M>.Edit;
begin
  inherited;
  FDataSetBase.Edit;
end;

function TContainerDataSetStrategy<M>.Find: TObjectList<M>;
begin
  Result := FDataSetBase.Find;
end;

function TContainerDataSetStrategy<M>.Find(AID: TValue): M;
begin
  Result := FDataSetBase.Find(AID);
end;

function TContainerDataSetStrategy<M>.Find(AWhere, AOrderBy: string): TObjectList<M>;
begin
  Result := FDataSetBase.Find(AWhere, AOrderBy);
end;

function TContainerDataSetStrategy<M>.GetDataSetInternal: TDataSet;
begin
  Result := FDataSetBase.FOrmDataSet;
end;

procedure TContainerDataSetStrategy<M>.Insert;
begin
  inherited;
  FDataSetBase.Insert;
end;

procedure TContainerDataSetStrategy<M>.Lazy(AOwner: M);
begin
  inherited;
  FDataSetBase.Lazy(AOwner);
end;

procedure TContainerDataSetStrategy<M>.NextPacket;
begin
  inherited;
  FDataSetBase.NextPacket;
end;

procedure TContainerDataSetStrategy<M>.Open;
begin
  inherited;
  FDataSetBase.Open;
end;

procedure TContainerDataSetStrategy<M>.Open(AWhere, AOrderBy: string);
begin
  inherited;
  FDataSetBase.Open(AWhere, AOrderBy);
end;

procedure TContainerDataSetStrategy<M>.Open(AID: TValue);
begin
  inherited;
  FDataSetBase.Open(AID);
end;

procedure TContainerDataSetStrategy<M>.OpenWhere(AWhere: string);
begin
  inherited;
  FDataSetBase.Open(AWhere);
end;

procedure TContainerDataSetStrategy<M>.Post;
begin
  inherited;
  FDataSetBase.Post;
end;

procedure TContainerDataSetStrategy<M>.Save(AObject: M);
begin
  inherited;
  FDataSetBase.Save(AObject);
end;

end.
