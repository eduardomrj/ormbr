{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dataset.adapter;

interface

uses
  Classes,
  SysUtils,
  StrUtils,
  Generics.Collections,
  Windows,
  DB,
  Rtti,
  TypInfo,
  ormbr.criteria,
  /// orm
  ormbr.dataset.bind,
  ormbr.dataset.events,
  ormbr.dataset.abstract,
  ormbr.session.manager,
  ormbr.factory.interfaces,
  ormbr.mapping.classes,
  ormbr.rtti.helper,
  ormbr.mapping.attributes,
  ormbr.types.mapping;

type
  /// <summary>
  /// M - Object M
  /// </summary>
  TDataSetAdapter<M: class, constructor> = class(TDataSetAbstract<M>)
  private
    /// <summary>
    /// Objeto para captura dos eventos do dataset passado pela interface
    /// </summary>
    FOrmDataSetEvents: TDataSet;
    /// <summary>
    /// Objeto para controle de estado do registro
    /// </summary>
    FOrmDataSource: TDataSource;
    /// <summary>
    /// Usado em relacionamento mestre-detalhe, guarda qual objeto pai
    /// </summary>
    FOwnerMasterObject: TObject;
    /// <summary>
    /// Controle de pagina��o vindo do banco de dados
    /// </summary>
    FPageSize: Integer;
    /// <summary>
    /// Classe para controle de evento interno com os eventos da interface do dataset
    /// </summary>
    FDataSetEvents: TDataSetEvents;
    ///
    procedure OpenDataSetChilds;
    procedure RefreshDataSetOneToOneChilds(AFieldName: string);
    procedure SetMasterObject(const AValue: TObject);
    procedure DoStateChange(Sender: TObject);
    procedure DoDataChange(Sender: TObject; Field: TField);
    procedure DoUpdateData(Sender: TObject);
    procedure FillMastersClass(ADatasetBase: TDataSetAdapter<M>; AObject: M);
    procedure ExecuteOneToOne(AObject: M; oProperty: TRttiProperty;
      ADatasetBase: TDataSetAdapter<M>);
    procedure ExecuteOneToMany(AObject: M; oProperty: TRttiProperty;
      ADatasetBase: TDataSetAdapter<M>; oRttiType: TRttiType);
    procedure GetMasterValues;
    ///
    function GetRelationFields(ATable: TTableMapping; ADetail: TDataSetAdapter<M>;
      var ASQLBuild: ICriteria): Boolean;
    function FindEvents(AEventName: string): Boolean;
    function GetAutoNextPacket: Boolean;
    procedure SetAutoNextPacket(const Value: Boolean);
    function GetCurrentRecordInternal: M;
  protected
    /// <summary>
    /// Objeto interface com o DataSet passado pela interface.
    /// </summary>
    FOrmDataSet: TDataSet;
    /// <summary>
    /// Uso interno para fazer mapeamento do registro dataset
    /// </summary>
    FCurrentRecordInternal: M;
    FMasterObject: TDictionary<string, TDataSetAdapter<M>>;
    FLookupsField: TList<TDataSetAdapter<M>>;
    FSession: TSessionDataSet<M>;
    FInternalIndex: Integer;
    FAutoNextPacket: Boolean;
    FConnection: IDBConnection;
    procedure OpenInternal(ASQL: string; AID: TValue); virtual;
    procedure DoBeforeScroll(DataSet: TDataSet); virtual;
    procedure DoAfterScroll(DataSet: TDataSet); virtual;
    procedure DoBeforeOpen(DataSet: TDataSet); virtual;
    procedure DoAfterOpen(DataSet: TDataSet); virtual;
    procedure DoBeforeClose(DataSet: TDataSet); virtual;
    procedure DoAfterClose(DataSet: TDataSet); virtual;
    procedure DoBeforeDelete(DataSet: TDataSet); virtual;
    procedure DoAfterDelete(DataSet: TDataSet); virtual;
    procedure DoBeforePost(DataSet: TDataSet); virtual;
    procedure DoAfterPost(DataSet: TDataSet); virtual;
    procedure DoBeforeCancel(DataSet: TDataSet); virtual;
    procedure DoAfterCancel(DataSet: TDataSet); virtual;
    procedure DoNewRecord(DataSet: TDataSet); virtual;
    procedure EmptyDataSetChilds; virtual; abstract;
    procedure GetDataSetEvents; virtual;
    procedure SetDataSetEvents; virtual;
    procedure DisableDataSetEvents;
    procedure EnableDataSetEvents;
    procedure ApplyInserter(MaxErros: Integer); virtual; abstract;
    procedure ApplyUpdater(MaxErros: Integer); virtual; abstract;
    procedure ApplyDeleter(MaxErros: Integer); virtual; abstract;
    procedure ApplyInternal(MaxErros: Integer); virtual; abstract;
    procedure RefreshRecord;
    procedure PopularDataSet(ADBResultSet: IDBResultSet);
    /// Comandos DataSet
    procedure Lazy(AOwner: M);
    procedure Open(AID: TValue); overload; virtual;
    procedure Open; overload; virtual;
    procedure Open(AWhere: string; AOrderBy: string = ''); overload; virtual;
    procedure OpenWhere(AWhere: string); deprecated 'Use Open()';
    procedure Insert; virtual;
    procedure Append; virtual;
    procedure Post; virtual;
    procedure Edit; virtual;
    procedure Delete; virtual;
    procedure Close; virtual;
    procedure Cancel; virtual;
    procedure CancelUpdates; virtual;
    procedure Save(AObject: M); virtual;
    procedure ApplyUpdates(MaxErros: Integer); virtual; abstract;
    procedure AddCalcField(AFieldName: string;
                           AFieldType: TFieldType;
                           ASize: Integer = 0;
                           AAlignment: TAlignment = taLeftJustify;
                           ADisplayFormat: string = '');
    procedure AddLookupField(AFieldName: string;
                             AKeyFields: string;
                             ALookupDataSet: TObject;
                             ALookupKeyFields: string;
                             ALookupResultField: string);
    procedure AddAggregateField(AFieldName: string;
                                AExpression: string;
                                AAlignment: TAlignment = taLeftJustify;
                                ADisplayFormat: string = '');
    procedure NextPacket;
    /// ObjectSet
    function Find: TObjectList<M>; overload; virtual;
    function Find(AID: TValue): M; overload; virtual;
    function Find(AWhere: string; AOrderBy: string = ''): TObjectList<M>; overload; virtual;
    /// <summary>
    /// Uso na interface para ler, gravar e alterar dados do registro atual no dataset, pelo objeto.
    /// </summary>
    property Current: M read GetCurrentRecordInternal;
    property AutoNextPacket: Boolean read GetAutoNextPacket write SetAutoNextPacket;
  public
    constructor Create(AConnection: IDBConnection; ADataSet:
      TDataSet; APageSize: Integer; AMasterObject: TObject); virtual;
    destructor Destroy; override;
  end;

implementation

uses
  ormbr.mapping.rttiutils,
  ormbr.dataset.fields,
  ormbr.mapping.explorer;

{ TDataSetAdapter<M> }

constructor TDataSetAdapter<M>.Create(AConnection: IDBConnection;
  ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject);
begin
  FConnection := AConnection;
  FOrmDataSet := ADataSet;
  FPageSize := APageSize;
  FSession := TSessionDataSet<M>.Create(AConnection, FPageSize);
  FOrmDataSetEvents := TDataSet.Create(nil);
  FMasterObject := TDictionary<string, TDataSetAdapter<M>>.Create;
  FLookupsField := TList<TDataSetAdapter<M>>.Create;
  FCurrentRecordInternal := M.Create;
  FOrmDataSource := TDataSource.Create(nil);
  FOrmDataSource.DataSet := FOrmDataSet;
  FOrmDataSource.OnDataChange  := DoDataChange;
  FOrmDataSource.OnStateChange := DoStateChange;
  FOrmDataSource.OnUpdateData  := DoUpdateData;
  TBindDataSet.GetInstance.SetInternalInitFieldDefsObjectClass(ADataSet, FCurrentRecordInternal);
  TBindDataSet.GetInstance.SetDataDictionary(ADataSet, FCurrentRecordInternal);
  FDataSetEvents := TDataSetEvents.Create;
  FAutoNextPacket := True;
  /// <summary>
  /// Vari�vel que identifica o campo que guarda o estado do registro.
  /// </summary>
  FInternalIndex := 0;
  if AMasterObject <> nil then
    SetMasterObject(AMasterObject);
end;

procedure TDataSetAdapter<M>.Save(AObject: M);
begin
  /// <summary>
  /// Aualiza o DataSet com os dados a vari�vel interna
  /// </summary>
  FOrmDataSet.Edit;
  TBindDataSet.GetInstance.SetPropertyToField(AObject, FOrmDataSet);
  FOrmDataSet.Post;
end;

destructor TDataSetAdapter<M>.Destroy;
var
  iLookup: Integer;
begin
  FOrmDataSet  := nil;
  FOwnerMasterObject := nil;
  FSession.Free;
  FOrmDataSource.Free;
  FDataSetEvents.Free;
  FOrmDataSetEvents.Free;
  FCurrentRecordInternal.Free;
  FMasterObject.Clear;
  FMasterObject.Free;
  if Assigned(FLookupsField) then
    for iLookup := 0 to FLookupsField.Count -1 do
      FLookupsField.Items[iLookup].Close;
  FLookupsField.Clear;
  FLookupsField.Free;
  inherited;
end;

procedure TDataSetAdapter<M>.Cancel;
begin
  FOrmDataSet.Cancel;
end;

procedure TDataSetAdapter<M>.CancelUpdates;
begin
  FSession.ModifiedFields.Clear;
end;

procedure TDataSetAdapter<M>.Close;
begin
  FOrmDataSet.Close;
end;

procedure TDataSetAdapter<M>.AddAggregateField(AFieldName: string;
                                               AExpression: string;
                                               AAlignment: TAlignment;
                                               ADisplayFormat: string);
begin
  TFieldSingleton.GetInstance.AddAggregateField(FOrmDataSet,
                                                AFieldName,
                                                AExpression,
                                                AAlignment,
                                                ADisplayFormat);
end;

procedure TDataSetAdapter<M>.AddCalcField(AFieldName: String;
                                          AFieldType: TFieldType;
                                          ASize: Integer;
                                          AAlignment: TAlignment;
                                          ADisplayFormat: string);
begin
  TFieldSingleton.GetInstance.AddCalcField(FOrmDataSet,
                                           AFieldName,
                                           AFieldType,
                                           ASize,
                                           AAlignment,
                                           ADisplayFormat);

end;

procedure TDataSetAdapter<M>.AddLookupField(AFieldName: string;
                                            AKeyFields: string;
                                            ALookupDataSet: TObject;
                                            ALookupKeyFields: string;
                                            ALookupResultField: string);
var
  oColumn: TColumnMapping;
  oColumns: TColumnMappingList;
begin
  /// Guarda o datasetlookup em uma lista para controle interno
  FLookupsField.Add(TDataSetAdapter<M>(ALookupDataSet));
  oColumns := FSession.Explorer.GetMappingColumn(FLookupsField.Last.FCurrentRecordInternal.ClassType);
  if oColumns <> nil then
  begin
    for oColumn in oColumns do
    begin
      if oColumn.ColumnName = ALookupResultField then
      begin
        TFieldSingleton.GetInstance.AddLookupField(AFieldName,
                                                   FOrmDataSet,
                                                   AKeyFields,
                                                   FLookupsField.Last.FOrmDataSet,
                                                   ALookupKeyFields,
                                                   ALookupResultField,
                                                   oColumn.FieldType,
                                                   oColumn.Size);
        /// <summary>
        /// Abre a tabela do TLookupField
        /// </summary>
        FLookupsField.Last.OpenInternal('', -1);
      end;
    end;
  end;
end;

procedure TDataSetAdapter<M>.Append;
begin
  FOrmDataSet.Append;
end;

procedure TDataSetAdapter<M>.EnableDataSetEvents;
var
  oClassType: TRttiType;
  oProperty: TRttiProperty;
  oPropInfo: PPropInfo;
  oMethod: TMethod;
  oMethodNil: TMethod;
begin
  oClassType := TRttiSingleton.GetInstance.GetRttiType(FOrmDataSet.ClassType);
  for oProperty in oClassType.GetProperties do
  begin
    if oProperty.PropertyType.TypeKind = tkMethod then
    begin
      if FindEvents(oProperty.Name) then
      begin
        oPropInfo := GetPropInfo(FOrmDataSet, oProperty.Name);
        if oPropInfo <> nil then
        begin
           oMethod := GetMethodProp(FOrmDataSetEvents, oPropInfo);
           if Assigned(oMethod.Code) then
           begin
              oMethodNil.Code := nil;
              SetMethodProp(FOrmDataSet, oPropInfo, oMethod);
              SetMethodProp(FOrmDataSetEvents, oPropInfo, oMethodNil);
           end;
        end;
      end;
    end;
  end;
end;

procedure TDataSetAdapter<M>.FillMastersClass(ADatasetBase: TDataSetAdapter<M>; AObject: M);
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oAttrProperty: TCustomAttribute;
begin
  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AObject.ClassType);
  for oProperty in oRttiType.GetProperties do
  begin
    for oAttrProperty in oProperty.GetAttributes do
    begin
      if oAttrProperty is Association then // Association
      begin
        if Association(oAttrProperty).Multiplicity in [OneToOne, ManyToOne] then
          ExecuteOneToOne(AObject, oProperty, ADatasetBase)
        else
        if Association(oAttrProperty).Multiplicity in [OneToMany, ManyToMany] then
          ExecuteOneToMany(AObject, oProperty, ADatasetBase, oRttiType);
      end;
    end;
  end;
end;

procedure TDataSetAdapter<M>.ExecuteOneToOne(AObject: M; oProperty: TRttiProperty;
  ADatasetBase: TDataSetAdapter<M>);
var
  oBookMark: TBookmark;
begin
  if ADatasetBase.FCurrentRecordInternal.ClassType = oProperty.PropertyType.AsInstance.MetaclassType then
  begin
    oBookMark := ADatasetBase.FOrmDataSet.Bookmark;
    while not ADatasetBase.FOrmDataSet.Eof do
    begin
      /// Popula o objeto M e o adiciona na lista e objetos com o registro do DataSet.
      TBindDataSet.GetInstance.SetFieldToProperty(ADatasetBase.FOrmDataSet,
                                                  oProperty.GetNullableValue(TObject(AObject)).AsObject);
      /// Pr�ximo registro
      ADatasetBase.FOrmDataSet.Next;
    end;
    ADatasetBase.FOrmDataSet.GotoBookmark(oBookMark);
    ADatasetBase.FOrmDataSet.FreeBookmark(oBookMark);
  end;
end;

procedure TDataSetAdapter<M>.ExecuteOneToMany(AObject: M; oProperty: TRttiProperty;
  ADatasetBase: TDataSetAdapter<M>; oRttiType: TRttiType);
var
  oPropertyType: TRttiType;
  oBookMark: TBookmark;
  oObjectType: TObject;
  oObjectList: TObject;
begin
  oPropertyType := oRttiType.GetProperty(oProperty.Name).PropertyType;
  oPropertyType := TRttiSingleton.GetInstance.GetListType(oPropertyType);
  if not oPropertyType.IsInstance then
    raise Exception.Create('Not in instance ' + oPropertyType.Parent.ClassName + ' - ' + oPropertyType.Name);
  ///
  if ADatasetBase.FCurrentRecordInternal.ClassType = oPropertyType.AsInstance.MetaclassType then
  begin
    oBookMark := ADatasetBase.FOrmDataSet.Bookmark;
    while not ADatasetBase.FOrmDataSet.Eof do
    begin
      oObjectType := oPropertyType.AsInstance.MetaclassType.Create;
      /// Popula o objeto M e o adiciona na lista e objetos com o registro do DataSet.
      TBindDataSet.GetInstance.SetFieldToProperty(ADatasetBase.FOrmDataSet, oObjectType);
      ///
      oObjectList := oProperty.GetNullableValue(TObject(AObject)).AsObject;
      TRttiSingleton.GetInstance.MethodCall(oObjectList, 'Add', [oObjectType]);
      /// Pr�ximo registro
      ADatasetBase.FOrmDataSet.Next;
    end;
    ADatasetBase.FOrmDataSet.GotoBookmark(oBookMark);
    ADatasetBase.FOrmDataSet.FreeBookmark(oBookMark);
  end;
end;

procedure TDataSetAdapter<M>.DisableDataSetEvents;
var
  oClassType: TRttiType;
  oProperty: TRttiProperty;
  oPropInfo: PPropInfo;
  oMethod: TMethod;
  oMethodNil: TMethod;
begin
  oClassType := TRttiSingleton.GetInstance.GetRttiType(FOrmDataSet.ClassType);
  for oProperty in oClassType.GetProperties do
  begin
    if oProperty.PropertyType.TypeKind = tkMethod then
    begin
      if FindEvents(oProperty.Name) then
      begin
        oPropInfo := GetPropInfo(FOrmDataSet, oProperty.Name);
        if oPropInfo <> nil then
        begin
           oMethod := GetMethodProp(FOrmDataSet, oPropInfo);
           if Assigned(oMethod.Code) then
           begin
              oMethodNil.Code := nil;
              SetMethodProp(FOrmDataSet, oPropInfo, oMethodNil);
              SetMethodProp(FOrmDataSetEvents, oPropInfo, oMethod);
           end;
        end;
      end;
    end;
  end;
end;

function TDataSetAdapter<M>.Find: TObjectList<M>;
begin
  Result := FSession.Find;
end;

function TDataSetAdapter<M>.Find(AID: TValue): M;
begin
  Result := FSession.Find(AID);
end;

function TDataSetAdapter<M>.Find(AWhere, AOrderBy: string): TObjectList<M>;
begin
  Result := FSession.Find(AWhere, AOrderBy);
end;

function TDataSetAdapter<M>.FindEvents(AEventName: string): Boolean;
begin
  Result := MatchStr(AEventName, ['AfterCancel'   ,'AfterClose'   ,'AfterDelete' ,
                                  'AfterEdit'     ,'AfterInsert'  ,'AfterOpen'   ,
                                  'AfterPost'     ,'AfterRefresh' ,'AfterScroll' ,
                                  'BeforeCancel'  ,'BeforeClose'  ,'BeforeDelete',
                                  'BeforeEdit'    ,'BeforeInsert' ,'BeforeOpen'  ,
                                  'BeforePost'    ,'BeforeRefresh','BeforeScroll',
                                  'OnCalcFields'  ,'OnDeleteError','OnEditError' ,
                                  'OnFilterRecord','OnNewRecord'  ,'OnPostError']);
end;

procedure TDataSetAdapter<M>.DoAfterClose(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterClose) then
    FDataSetEvents.AfterClose(DataSet);
end;

procedure TDataSetAdapter<M>.DoAfterDelete(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterDelete) then
    FDataSetEvents.AfterDelete(DataSet);
end;

procedure TDataSetAdapter<M>.DoAfterOpen(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterOpen) then
    FDataSetEvents.AfterOpen(DataSet);
end;

procedure TDataSetAdapter<M>.DoAfterPost(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterPost) then
    FDataSetEvents.AfterPost(DataSet);
end;

procedure TDataSetAdapter<M>.DoAfterScroll(DataSet: TDataSet);
begin
  if DataSet.State in [dsBrowse] then
    OpenDataSetChilds;
  if Assigned(FDataSetEvents.AfterScroll) then
    FDataSetEvents.AfterScroll(DataSet);
  /// <summary>
  /// Controle de pagina��o de registros retornados do banco de dados
  /// </summary>
  if FPageSize > -1 then
    if FOrmDataSet.Eof then
      if FAutoNextPacket then
        NextPacket;
end;

procedure TDataSetAdapter<M>.Lazy(AOwner: M);
var
  oTable: TTableMapping;
  oCriteria: ICriteria;
begin
  if AOwner <> nil then
  begin
    if FOwnerMasterObject = nil then
    begin
      if not FOrmDataSet.Active then
      begin
        SetMasterObject(AOwner);
        oTable := FSession.Explorer.GetMappingTable(FCurrentRecordInternal.ClassType);
        if oTable <> nil then
        begin
          oCriteria := CreateCriteria.Select;
          /// <summary>
          /// Gera SELECT de abertura da tabela associada
          /// </summary>
          TDataSetAdapter<M>(FOwnerMasterObject).GetRelationFields(oTable, Self, oCriteria);
          Open(oCriteria.AsString);
        end;
      end;
    end;
  end
  else
  begin
    if FOwnerMasterObject <> nil then
    begin
      if TDataSetAdapter<M>(FOwnerMasterObject).FOrmDataSet.Active then
      begin
        SetMasterObject(nil);
        Close;
      end;
    end
  end;
end;

procedure TDataSetAdapter<M>.OpenDataSetChilds;
var
  oTable: TTableMapping;
  oChild: TPair<string, TDataSetAdapter<M>>;
  oCriteria: ICriteria;
begin
  if FOrmDataSet.Active then
  begin
     if FOrmDataSet.RecordCount > 0 then
     begin
        /// <summary>
        /// Se Count > 0 identifica-se que � o objeto � um Master
        /// </summary>
        if FMasterObject.Count > 0 then
        begin
           for oChild in FMasterObject do
           begin
              oTable := FSession.Explorer.GetMappingTable(oChild.Value.FCurrentRecordInternal.ClassType);
              if oTable <> nil then
              begin
                oChild.Value.Close;
                oCriteria := CreateCriteria.Select;
                /// <summary>
                /// Gera SELECT de abertura da tabela associada
                /// </summary>
                GetRelationFields(oTable, oChild.Value, oCriteria);
                oChild.Value.OpenInternal(oCriteria.AsString, -1);
              end;
           end;
        end;
     end;
  end;
end;

function TDataSetAdapter<M>.GetRelationFields(ATable: TTableMapping;
  ADetail: TDataSetAdapter<M>; var ASQLBuild: ICriteria): Boolean;
var
  oAssociations: TAssociationMappingList;
  oAssociation: TAssociationMapping;
  oColumn: string;
  iFor: Integer;
begin
   Result := False;
   oAssociations := FSession.Explorer.GetMappingAssociation(FCurrentRecordInternal.ClassType);
   if oAssociations <> nil then
   begin
      for oAssociation in oAssociations do
      begin
         /// <summary>
         /// Verifica��o se tem algum mapeamento OneToOne para a classe.
         /// </summary>
         if oAssociation.ClassNameRef = ADetail.FCurrentRecordInternal.ClassName then
         begin
            /// <summary>
            /// Verifica��o se foi relacionado uma lista de campos para o Select, caso n�o, Select All
            /// </summary>
            if oAssociation.ColumnsSelectRef.Count > 0 then
            begin
              for iFor := 0 to oAssociation.ColumnsNameRef.Count -1 do
                ASQLBuild.Column(ATable.Name + '.' + oAssociation.ColumnsNameRef[iFor]);
              for oColumn in oAssociation.ColumnsSelectRef do
                ASQLBuild.Column(ATable.Name + '.' + oColumn);
            end
            else
              ASQLBuild.All;
            /// <summary>
            /// From pelo nome da classe de referencia e aplicado o Where pela Coluna de referencia.
            /// </summary>
            ASQLBuild.From(ATable.Name);
            for iFor := 0 to oAssociation.ColumnsNameRef.Count -1 do
              ASQLBuild.Where(ATable.Name + '.' +
                              oAssociation.ColumnsNameRef[iFor] + '=' +
                              TBindDataSet.GetInstance.GetFieldValue(FOrmDataSet,
                                                                     oAssociation.ColumnsName[iFor],
                                                                     FOrmDataSet.FieldByName(oAssociation.ColumnsName[iFor]).DataType));
            Result := True;
         end;
      end;
   end;
end;

procedure TDataSetAdapter<M>.Insert;
begin
  FOrmDataSet.Insert;
end;

procedure TDataSetAdapter<M>.DoBeforeCancel(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeCancel) then
    FDataSetEvents.BeforeCancel(DataSet);
end;

procedure TDataSetAdapter<M>.DoAfterCancel(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterCancel) then
    FDataSetEvents.AfterCancel(DataSet);
end;

procedure TDataSetAdapter<M>.DoBeforeClose(DataSet: TDataSet);
var
  oChild: TPair<string, TDataSetAdapter<M>>;
  oLookup: TDataSetAdapter<M>;
begin
  /// <summary>
  /// Fecha todas as tabelas relacionadas mastar
  /// </summary>
  if Assigned(FMasterObject) then
    if FMasterObject.Count > 0 then
      for oChild in FMasterObject do
        oChild.Value.Close;

  if Assigned(FDataSetEvents.BeforeClose) then
    FDataSetEvents.BeforeClose(DataSet);
end;

procedure TDataSetAdapter<M>.DoBeforeDelete(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeDelete) then
    FDataSetEvents.BeforeDelete(DataSet);
  /// <summary>
  /// Alimenta a lista com registros deletados
  /// </summary>
  FSession.DeleteList.Add(M.Create);
  TBindDataSet.GetInstance.SetFieldToProperty(FOrmDataSet, FSession.DeleteList.Last);
  /// <summary>
  /// Deleta registros de todos os DataSet filhos
  /// </summary>
  EmptyDataSetChilds;
end;

procedure TDataSetAdapter<M>.DoBeforeOpen(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeOpen) then
    FDataSetEvents.BeforeOpen(DataSet);
end;

procedure TDataSetAdapter<M>.DoBeforePost(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforePost) then
    FDataSetEvents.BeforePost(DataSet);
end;

procedure TDataSetAdapter<M>.DoBeforeScroll(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeScroll) then
    FDataSetEvents.BeforeScroll(DataSet);
end;

procedure TDataSetAdapter<M>.DoDataChange(Sender: TObject; Field: TField);
begin
  if FOrmDataSet.State in [dsEdit] then
  begin
    if Field <> nil then
    begin
      if Field.FieldKind = fkData then
      begin
        if Field.FieldName <> cInternalField then
        begin
          if FSession.ModifiedFields.IndexOf(Field.FieldName) = -1 then
            FSession.ModifiedFields.Add(Field.FieldName);
          /// <summary>
          /// Atualiza o registro da tabela externa, se o campo alterado
          /// pertencer a um relacionamento OneToOne ou ManyToOne
          /// </summary>
          RefreshDataSetOneToOneChilds(Field.FieldName);
        end;
      end;
    end;
  end;
end;

procedure TDataSetAdapter<M>.DoNewRecord(DataSet: TDataSet);
begin
  /// <summary>
  /// Limpa os datasets em mem�ria para receberem novos valores
  /// </summary>
  EmptyDataSetChilds;
  /// <summary>
  /// Busca valor da tabela master, caso aqui seja uma tabela detalhe.
  /// </summary>
  GetMasterValues;
  if Assigned(FDataSetEvents.OnNewRecord) then
    FDataSetEvents.OnNewRecord(DataSet);
end;

procedure TDataSetAdapter<M>.DoStateChange(Sender: TObject);
begin
   if FOrmDataSet.State in [dsInsert] then
     FOrmDataSet.Fields[FInternalIndex].AsInteger := Integer(FOrmDataSet.State)
   else
   if FOrmDataSet.State in [dsEdit] then
     if FOrmDataSet.Fields[FInternalIndex].AsInteger = -1 then
       FOrmDataSet.Fields[FInternalIndex].AsInteger := Integer(FOrmDataSet.State);
end;

procedure TDataSetAdapter<M>.DoUpdateData(Sender: TObject);
begin

end;

procedure TDataSetAdapter<M>.Delete;
begin
  FOrmDataSet.Delete;
end;

procedure TDataSetAdapter<M>.Edit;
begin
  FOrmDataSet.Edit;
end;

procedure TDataSetAdapter<M>.GetDataSetEvents;
begin
  /// Scroll Events
  if Assigned(FOrmDataSet.BeforeScroll) then FDataSetEvents.BeforeScroll := FOrmDataSet.BeforeScroll;
  if Assigned(FOrmDataSet.AfterScroll)  then FDataSetEvents.AfterScroll  := FOrmDataSet.AfterScroll;
  /// Open Events
  if Assigned(FOrmDataSet.BeforeOpen)  then FDataSetEvents.BeforeOpen := FOrmDataSet.BeforeOpen;
  if Assigned(FOrmDataSet.AfterOpen)   then FDataSetEvents.AfterOpen  := FOrmDataSet.AfterOpen;
  /// Close Events
  if Assigned(FOrmDataSet.BeforeClose)  then FDataSetEvents.BeforeClose := FOrmDataSet.BeforeClose;
  if Assigned(FOrmDataSet.AfterClose)   then FDataSetEvents.AfterClose  := FOrmDataSet.AfterClose;
  /// Delete Events
  if Assigned(FOrmDataSet.BeforeDelete) then FDataSetEvents.BeforeDelete := FOrmDataSet.BeforeDelete;
  if Assigned(FOrmDataSet.AfterDelete)  then FDataSetEvents.AfterDelete  := FOrmDataSet.AfterDelete;
  /// Delete Events
  if Assigned(FOrmDataSet.BeforePost) then FDataSetEvents.BeforePost := FOrmDataSet.BeforePost;
  if Assigned(FOrmDataSet.AfterPost)  then FDataSetEvents.AfterPost  := FOrmDataSet.AfterPost;
  /// Delete Events
  if Assigned(FOrmDataSet.BeforeCancel) then FDataSetEvents.BeforeCancel := FOrmDataSet.BeforeCancel;
  if Assigned(FOrmDataSet.AfterCancel)  then FDataSetEvents.AfterCancel  := FOrmDataSet.AfterCancel;
  /// NewRecord Events
  if Assigned(FOrmDataSet.OnNewRecord)  then FDataSetEvents.OnNewRecord := FOrmDataSet.OnNewRecord;
end;

function TDataSetAdapter<M>.GetAutoNextPacket: Boolean;
begin
  Result := FAutoNextPacket;
end;

function TDataSetAdapter<M>.GetCurrentRecordInternal: M;
var
  oChild: TPair<string, TDataSetAdapter<M>>;
begin
  if FOrmDataSet.Active then
  begin
    if FOrmDataSet.RecordCount > 0 then
     begin
       TBindDataSet.GetInstance.SetFieldToProperty(FOrmDataSet, TObject(FCurrentRecordInternal));
       for oChild in FMasterObject do
         oChild.Value.FillMastersClass(oChild.Value, FCurrentRecordInternal);
     end;
  end;
  Result := FCurrentRecordInternal;
end;

procedure TDataSetAdapter<M>.OpenInternal(ASQL: String; AID: TValue);
var
  oDBResultSet: IDBResultSet;
begin
  if AID.AsInteger > -1 then
    oDBResultSet := FSession.Open(AID)
  else
    oDBResultSet := FSession.Open(ASQL);
  /// <summary>
  /// Popula o DataSet em mem�ria com os registros retornardos no comando SQL
  /// </summary>
  PopularDataSet(oDBResultSet);
end;

procedure TDataSetAdapter<M>.OpenWhere(AWhere: string);
begin
  Open(AWhere);
end;

procedure TDataSetAdapter<M>.NextPacket;
var
  oDBResultSet: IDBResultSet;
  oBookMark: TBookmark;
begin
  if not FSession.FetchingRecords then
  begin
     FOrmDataSet.DisableControls;
     DisableDataSetEvents;
     oBookMark := FOrmDataSet.Bookmark;
     try
       oDBResultSet := FSession.NextPacket;
       /// <summary>
       /// Popula o DataSet em mem�ria com os registros retornardos no comando SQL
       /// </summary>
       PopularDataSet(oDBResultSet);
     finally
       FOrmDataSet.GotoBookmark(oBookMark);
       FOrmDataSet.EnableControls;
       EnableDataSetEvents;
     end;
  end;
end;

procedure TDataSetAdapter<M>.Open;
begin
  OpenInternal('', -1);
end;

procedure TDataSetAdapter<M>.Open(AID: TValue);
begin
  OpenInternal('', AID);
end;

procedure TDataSetAdapter<M>.Open(AWhere: string; AOrderBy: string);
begin
  OpenInternal(FSession.OpenWhere(AWhere, AOrderBy), -1);
end;

procedure TDataSetAdapter<M>.Post;
begin
  FOrmDataSet.Post;
end;

procedure TDataSetAdapter<M>.RefreshDataSetOneToOneChilds(AFieldName: string);
var
  oTable: TTableMapping;
  oAssociations: TAssociationMappingList;
  oAssociation: TAssociationMapping;
  oDataSetChild: TDataSetAdapter<M>;
  oCriteria: ICriteria;
begin
  if FOrmDataSet.Active then
  begin
    if FOrmDataSet.RecordCount > 0 then
    begin
      oAssociations := FSession.Explorer.GetMappingAssociation(FCurrentRecordInternal.ClassType);
      if oAssociations <> nil then
      begin
        for oAssociation in oAssociations do
        begin
          if oAssociation.ColumnsName.IndexOf(AFieldName) > -1 then
          begin
            if oAssociation.Multiplicity in [OneToOne, ManyToOne] then
            begin
              oDataSetChild := FMasterObject.Items[oAssociation.ClassNameRef];
              if oDataSetChild <> nil then
              begin
                oTable := FSession.Explorer.GetMappingTable(oDataSetChild.FCurrentRecordInternal.ClassType);
                if oTable <> nil then
                begin
                  oDataSetChild.Close;
                  oCriteria := CreateCriteria.Select;
                  /// <summary>
                  /// Gera SELECT de abertura da tabela associada
                  /// </summary>
                  GetRelationFields(oTable, oDataSetChild, oCriteria);
                  oDataSetChild.OpenInternal(oCriteria.AsString, -1);
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TDataSetAdapter<M>.RefreshRecord;
var
  oDBResultSet: IDBResultSet;
  oPrimaryKey: TPrimaryKeyMapping;
begin
  oPrimaryKey := FSession.Explorer.GetMappingPrimaryKey(FCurrentRecordInternal.ClassType);
  if oPrimaryKey <> nil then
  begin
    FOrmDataSet.DisableControls;
    DisableDataSetEvents;
    try
      oDBResultSet := FSession.Open(FOrmDataSet.FieldByName(oPrimaryKey.Columns[0]).AsInteger);
      /// Popula DataSet
      while oDBResultSet.NotEof do
      begin
        FOrmDataSet.Edit;
        TBindDataSet.GetInstance.SetFieldToField(oDBResultSet, FOrmDataSet);
        FOrmDataSet.Post;
      end;
    finally
      FOrmDataSet.EnableControls;
      EnableDataSetEvents;
    end;
  end;
end;

procedure TDataSetAdapter<M>.PopularDataSet(ADBResultSet: IDBResultSet);
begin
//           FOrmDataSet.Locate(KeyFiels, KeyValues, Options);
//          { TODO -oISAQUE : Procurar forma de verificar se o registro n�o j� est� em mem�ria
//                            pela chave primaria }
  while ADBResultSet.NotEof do
  begin
     FOrmDataSet.Append;
     TBindDataSet.GetInstance.SetFieldToField(ADBResultSet, FOrmDataSet);
     FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
     FOrmDataSet.Post;
  end;
  ADBResultSet.Close;
end;

procedure TDataSetAdapter<M>.SetAutoNextPacket(const Value: Boolean);
begin
  FAutoNextPacket := Value;
end;

procedure TDataSetAdapter<M>.SetDataSetEvents;
begin
   FOrmDataSet.BeforeScroll := DoBeforeScroll;
   FOrmDataSet.AfterScroll  := DoAfterScroll;
   FOrmDataSet.BeforeClose  := DoBeforeClose;
   FOrmDataSet.BeforeOpen   := DoBeforeOpen;
   FOrmDataSet.AfterOpen    := DoAfterOpen;
   FOrmDataSet.AfterClose   := DoAfterClose;
   FOrmDataSet.BeforeDelete := DoBeforeDelete;
   FOrmDataSet.AfterDelete  := DoAfterDelete;
   FOrmDataSet.BeforePost   := DoBeforePost;
   FOrmDataSet.AfterPost    := DoAfterPost;
   FOrmDataSet.OnNewRecord  := DoNewRecord;
end;

procedure TDataSetAdapter<M>.GetMasterValues;
var
  oOneToMany: TAssociationMapping;
  oOneToManyList: TAssociationMappingList;
  oCurrentMaster: TDataSetAdapter<M>;
  iFor: Integer;
begin
  if Assigned(FOwnerMasterObject) then
  begin
    oCurrentMaster := TDataSetAdapter<M>(FOwnerMasterObject);
    oOneToManyList := FSession.Explorer.GetMappingAssociation(oCurrentMaster.FCurrentRecordInternal.ClassType);
    if oOneToManyList <> nil then
    begin
      for oOneToMany in oOneToManyList do
        if oOneToMany.Multiplicity in [OneToMany, ManyToMany] then
          for iFor := 0 to oOneToMany.ColumnsName.Count -1 do
            FOrmDataSet.FieldByName(oOneToMany.ColumnsNameRef[iFor]).Value := oCurrentMaster.FOrmDataSet.FieldByName(oOneToMany.ColumnsName[iFor]).Value;
    end;
  end;
end;

procedure TDataSetAdapter<M>.SetMasterObject(const AValue: TObject);
begin
  if FOwnerMasterObject <> AValue then
  begin
    if FOwnerMasterObject <> nil then
      if TDataSetAdapter<M>(FOwnerMasterObject).FMasterObject.ContainsKey(FCurrentRecordInternal.ClassName) then
        TDataSetAdapter<M>(FOwnerMasterObject).FMasterObject.Remove(FCurrentRecordInternal.ClassName);

    if AValue <> nil then
      TDataSetAdapter<M>(AValue).FMasterObject.Add(FCurrentRecordInternal.ClassName, Self);

    FOwnerMasterObject := AValue;
  end;
end;

end.
