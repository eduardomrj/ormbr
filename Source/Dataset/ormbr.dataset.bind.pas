{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dataset.bind;

interface

uses
  Classes,
  SysUtils,
  Rtti,
  DB,
  TypInfo,
  Variants,
  /// orm
  ormbr.mapping.attributes,
  ormbr.mapping.rttiutils,
  ormbr.mapping.exceptions,
  ormbr.factory.interfaces,
  ormbr.rtti.helper,
  ormbr.objects.helper,
  ormbr.types.nullable;

type
  IBindDataSet = interface
    ['{8EAF6052-177E-4D4B-9E0A-386799C129FC}']
    procedure SetDataDictionary(ADataSet: TDataSet; AObject: TObject);
    procedure SetInternalInitFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
    procedure SetPropertyToField(AObject: TObject; ADataSet: TDataSet);
    procedure SetFieldToProperty(ADataSet: TDataSet; AObject: TObject);
    procedure SetFieldToField(AResultSet: IDBResultSet; ADataSet: TDataSet);
    function GetFieldValue(ADataSet: TDataSet; AFieldName: string; AFieldType: TFieldType): string;
  end;

  TBindDataSet = class(TInterfacedObject, IBindDataSet)
  private
  class var
    FInstance: IBindDataSet;
    FContext: TRttiContext;
    constructor CreatePrivate;
    procedure SetAggregateFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
    procedure SetCalcFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
    procedure SetAggregateFields(ADataSet: TDataSet; AObject: TObject);
  public
    { Public declarations }
    constructor Create;
    class function GetInstance: IBindDataSet;
    procedure SetDataDictionary(ADataSet: TDataSet; AObject: TObject);
    procedure SetInternalInitFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
    procedure SetPropertyToField(AObject: TObject; ADataSet: TDataSet);
    procedure SetFieldToProperty(ADataSet: TDataSet; AObject: TObject);
    procedure SetFieldToField(AResultSet: IDBResultSet; ADataSet: TDataSet);
    function GetFieldValue(ADataSet: TDataSet; AFieldName: string; AFieldType: TFieldType): string;
  end;

implementation

uses
  ormbr.dataset.fields,
  ormbr.types.mapping,
  ormbr.mapping.classes,
  ormbr.mapping.explorer,
  ormbr.types.blob;

{ TBindDataSet }

procedure TBindDataSet.SetPropertyToField(AObject: TObject; ADataSet: TDataSet);
var
  LColumn: TColumnMapping;
  LColumns: TColumnMappingList;
  LSameText: Boolean;
begin
  /// <summary>
  /// Busca lista de columnas do mapeamento
  /// </summary>
  LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AObject.ClassType);
  for LColumn in LColumns do
  begin
    if LColumn.IsJoinColumn then
      Continue;
    if LColumn.IsNoUpdate then
      Continue;
    LSameText := SameText(ADataSet.Fields[LColumn.FieldIndex +1].Value,
                          LColumn.PropertyRtti.GetNullableValue(AObject).AsVariant);
    if TRttiSingleton.GetInstance.IsNullable(LColumn.PropertyRtti.PropertyType.Handle) then
    begin
      if not LSameText then
        ADataSet.Fields[LColumn.FieldIndex +1].Value
          := LColumn.PropertyRtti.GetNullableValue(AObject).AsVariant
    end
    else
    if TRttiSingleton.GetInstance.IsBlob(LColumn.PropertyRtti.PropertyType.Handle) then
    begin
      if not LSameText then
        if ADataSet.Fields[LColumn.FieldIndex +1].IsBlob then
          TBlobField(ADataSet.Fields[LColumn.FieldIndex +1]).AsBytes
            := LColumn.PropertyRtti.GetNullableValue(AObject).AsType<TBlob>.ToBytes
        else
          raise Exception.Create(
            Format('Column [%s] must have blob value', [ADataSet.Fields[LColumn.FieldIndex +1].FieldName]));
    end
    else
    begin
      if not LSameText then
        ADataSet.Fields[LColumn.FieldIndex +1].Value
          := LColumn.PropertyRtti.GetNullableValue(AObject).AsVariant;
    end;
  end;
end;

constructor TBindDataSet.Create;
begin
   raise Exception.Create('Para usar o BindDataSet use o m�todo TBindDataSet.GetInstance()');
end;

constructor TBindDataSet.CreatePrivate;
begin
   inherited;
   FContext := TRttiContext.Create;
end;

function TBindDataSet.GetFieldValue(ADataSet: TDataSet; AFieldName: string;
  AFieldType: TFieldType): string;
begin
  case AFieldType of
    ftUnknown: ;
    ftString, ftDate, ftTime, ftDateTime, ftTimeStamp:
    begin
      Result := QuotedStr(ADataSet.FieldByName(AFieldName).AsString);
    end;
    ftInteger, ftSmallint, ftWord:
    begin
      Result := IntToStr(ADataSet.FieldByName(AFieldName).AsInteger);
    end;
    else
      Result := ADataSet.FieldByName(AFieldName).AsString;
{
   ftBoolean: ;
   ftFloat: ;
   ftCurrency: ;
   ftBCD: ;
   ftBytes: ;
   ftVarBytes: ;
   ftAutoInc: ;
   ftBlob: ;
   ftMemo: ;
   ftGraphic: ;
   ftFmtMemo: ;
   ftParadoxOle: ;
   ftDBaseOle: ;
   ftTypedBinary: ;
   ftCursor: ;
   ftFixedChar: ;
   ftWideString: ;
   ftLargeint: ;
   ftADT: ;
   ftArray: ;
   ftReference: ;
   ftDataSet: ;
   ftOraBlob: ;
   ftOraClob: ;
   ftVariant: ;
   ftInterface: ;
   ftIDispatch: ;
   ftGuid: ;
   ftFMTBcd: ;
   ftFixedWideChar: ;
   ftWideMemo: ;
   ftOraTimeStamp: ;
   ftOraInterval: ;
   ftLongWord: ;
   ftShortint: ;
   ftByte: ;
   ftExtended: ;
   ftConnection: ;
   ftParams: ;
   ftStream: ;
   ftTimeStampOffset: ;
   ftObject: ;
   ftSingle: ;
}
  end;
end;

class function TBindDataSet.GetInstance: IBindDataSet;
begin
   if not Assigned(FInstance) then
      FInstance := TBindDataSet.CreatePrivate;

   Result := FInstance;
end;

procedure TBindDataSet.SetAggregateFieldDefsObjectClass(ADataSet: TDataSet;
  AObject: TObject);
var
  LRttiType: TRttiType;
  LCalcs: TArray<TCustomAttribute>;
  LCalc: TCustomAttribute;
begin
  LRttiType := FContext.GetType(AObject.ClassType);
  LCalcs :=  LRttiType.GetAggregateField;
  for LCalc in LCalcs do
  begin
    TFieldSingleton.GetInstance.AddAggregateField(ADataSet,
                                                  AggregateField(LCalc).FieldName,
                                                  AggregateField(LCalc).Expression,
                                                  AggregateField(LCalc).Alignment,
                                                  AggregateField(LCalc).DisplayFormat);

  end;
end;

procedure TBindDataSet.SetAggregateFields(ADataSet: TDataSet; AObject: TObject);
var
  LRttiType: TRttiType;
  LAggregates: TArray<TCustomAttribute>;
  LAggregate: TCustomAttribute;
begin
  LRttiType := FContext.GetType(AObject.ClassType);
  LAggregates :=  LRttiType.GetAggregateField;
  for LAggregate in LAggregates do
  begin
    TFieldSingleton.GetInstance.AddAggregateField(ADataSet,
                                                  AggregateField(LAggregate).FieldName,
                                                  AggregateField(LAggregate).Expression,
                                                  AggregateField(LAggregate).Alignment,
                                                  AggregateField(LAggregate).DisplayFormat);

  end;
end;

procedure TBindDataSet.SetCalcFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
var
  LCalcField: TCalcFieldMapping;
  LCalcFields: TCalcFieldMappingList;
begin
  LCalcFields := TMappingExplorer.GetInstance.GetMappingCalcField(AObject.ClassType);
  if LCalcFields <> nil then
  begin
    for LCalcField in LCalcFields do
    begin
      TFieldSingleton.GetInstance.AddCalcField(ADataSet,
                                               LCalcField.FieldName,
                                               LCalcField.FieldType,
                                               LCalcField.Size,
                                               LCalcField.Alignment,
                                               LCalcField.DisplayFormat);
    end;
  end;
end;

procedure TBindDataSet.SetDataDictionary(ADataSet: TDataSet; AObject: TObject);
var
  LColumn: TColumnMapping;
  LColumns: TColumnMappingList;
  LAttributo: TCustomAttribute;
  LFieldIndex: Integer;
begin
   LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AObject.ClassType);
   for LColumn in LColumns do
   begin
     LFieldIndex := LColumn.FieldIndex + 1;
     if Assigned(TField(LColumn.PropertyRtti)) then
     begin
        LAttributo := LColumn.PropertyRtti.GetDictionary;
        if LAttributo = nil then
          Continue;

        /// DisplayLabel
        if Length(Dictionary(LAttributo).DisplayLabel) > 0 then
           ADataSet.Fields[LFieldIndex].DisplayLabel := Dictionary(LAttributo).DisplayLabel;

        /// ConstraintErrorMessage
        if Length(Dictionary(LAttributo).ConstraintErrorMessage) > 0 then
           ADataSet.Fields[LFieldIndex].ConstraintErrorMessage := Dictionary(LAttributo).ConstraintErrorMessage;

        /// DefaultExpression
        if Length(Dictionary(LAttributo).DefaultExpression) > 0 then
        begin
           if Dictionary(LAttributo).DefaultExpression = 'Date' then
              ADataSet.Fields[LFieldIndex].DefaultExpression := QuotedStr(DateToStr(Date))
           else
           if Dictionary(LAttributo).DefaultExpression = 'Now' then
              ADataSet.Fields[LFieldIndex].DefaultExpression := QuotedStr(DateTimeToStr(Now))
           else
              ADataSet.Fields[LFieldIndex].DefaultExpression := Dictionary(LAttributo).DefaultExpression;
        end;
        /// DisplayFormat
        if Length(Dictionary(LAttributo).DisplayFormat) > 0 then
           TDateField(ADataSet.Fields[LFieldIndex]).DisplayFormat := Dictionary(LAttributo).DisplayFormat;

        /// EditMask
        if Length(Dictionary(LAttributo).EditMask) > 0 then
           ADataSet.Fields[LFieldIndex].EditMask := Dictionary(LAttributo).EditMask;

        /// Alignment
        if Dictionary(LAttributo).Alignment in [taLeftJustify,taRightJustify,taCenter] then
           ADataSet.Fields[LFieldIndex].Alignment := Dictionary(LAttributo).Alignment;
     end;
   end;
end;

procedure TBindDataSet.SetFieldToProperty(ADataSet: TDataSet; AObject: TObject);
var
  LColumn: TColumnMapping;
  LColumns: TColumnMappingList;
  LFieldIndex: Integer;
  LSetValueBlob: TBlob;
begin
  LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AObject.ClassType);
  for LColumn in LColumns do
  begin
    /// <summary>
    /// S� passa valor � propriedade se ela estiver definida como escrita,
    /// propriedade definida como n�o escrita, � usada para campo calculado.
    /// <example>
    /// <code>
    /// function GetTotal: Double;
    /// property Total: Double read GetTotal;
    /// </code>
    /// <returns>Quantidade * Preco</returns>
    /// </example>
    /// </summary>
    if LColumn.PropertyRtti.IsWritable then
    begin
       LFieldIndex := LColumn.FieldIndex + 1;
       /// Required the restriction
  //     oRestriction := AObject.GetRestriction(oProperty);
  //     if oRestriction <> nil then
  //       if NotNull in Restrictions(oRestriction).Restrictions then
  //         if ADataSet.Fields[LFieldIndex].Value = Null then
  //           raise EFieldNotNull.Create(oProperty.Name);
      if LColumn.PropertyRtti.PropertyType.TypeKind in [tkString, tkUString] then
        LColumn.PropertyRtti.SetValue(AObject, ADataSet.Fields[LFieldIndex].AsString)
      else
      if LColumn.PropertyRtti.PropertyType.TypeKind in [tkFloat] then
      begin
        /// TDateTime
        if LColumn.PropertyRtti.PropertyType.Handle = TypeInfo(TDateTime) then
          LColumn.PropertyRtti.SetValue(AObject, ADataSet.Fields[LFieldIndex].AsDateTime)
        else
        /// TTime
        if LColumn.PropertyRtti.PropertyType.Handle = TypeInfo(TTime) then
          LColumn.PropertyRtti.SetValue(AObject, ADataSet.Fields[LFieldIndex].AsDateTime)
        else
          LColumn.PropertyRtti.SetValue(AObject, ADataSet.Fields[LFieldIndex].AsCurrency)
      end
      else
      if LColumn.PropertyRtti.PropertyType.TypeKind in [tkRecord] then
      begin
        /// TBlob
        if TRttiSingleton.GetInstance.IsBlob(LColumn.PropertyRtti.PropertyType.Handle) then
        begin
          if ADataSet.Fields[LFieldIndex].IsBlob then
          begin
            if not ADataSet.Fields[LFieldIndex].IsNull then
            begin
              LSetValueBlob.SetBlobField(TBlobField(ADataSet.Fields[LFieldIndex]));
              LColumn.PropertyRtti.SetValue(AObject, TValue.From<TBlob>(LSetValueBlob));

            end;
          end
          else
            raise Exception.Create(Format('Column [%s] must have blob value', [ADataSet.Fields[LFieldIndex].FieldName]));
        end;
        /// Nullable
        if TRttiSingleton.GetInstance.IsNullable(LColumn.PropertyRtti.PropertyType.Handle) then
          LColumn.PropertyRtti.SetNullableValue(AObject,
                                                LColumn.PropertyRtti.PropertyType.Handle,
                                                ADataSet.Fields[LFieldIndex].AsVariant);
      end
      else
      if LColumn.PropertyRtti.PropertyType.TypeKind in [tkInteger] then
        LColumn.PropertyRtti.SetValue(AObject, ADataSet.Fields[LFieldIndex].AsInteger)
    end;
  end;
end;

procedure TBindDataSet.SetFieldToField(AResultSet: IDBResultSet; ADataSet: TDataSet);
var
  LFor: Integer;
  LFieldValue: Variant;
  LReadOnly: Boolean;
begin
  for LFor := 1 to ADataSet.Fields.Count -1 do
  begin
     if (ADataSet.Fields[LFor].FieldKind = fkData) and
        (ADataSet.Fields[LFor].FieldName <> cInternalField) then
     begin
        LReadOnly := ADataSet.Fields[LFor].ReadOnly;
        ADataSet.Fields[LFor].ReadOnly := False;
        try
          if ADataSet.Fields[LFor].IsBlob then
          begin
            LFieldValue := AResultSet.GetFieldValue(LFor -1);
            if LFieldValue <> Variants.Null then
              ADataSet.Fields[LFor].AsBytes := LFieldValue;
          end
          else
            ADataSet.Fields[LFor].AsVariant := AResultSet.GetFieldValue(LFor -1);
        finally
          ADataSet.Fields[LFor].ReadOnly := LReadOnly;
        end;
     end;
  end;
end;

procedure TBindDataSet.SetInternalInitFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
var
  LColumn: TColumnMapping;
  LColumns: TColumnMappingList;
  LPrimaryKey: TPrimaryKeyMapping;
  LFor: Integer;
begin
  ADataSet.Close;
  ADataSet.FieldDefs.Clear;
  try
    LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AObject.ClassType);
    for LColumn in LColumns do
    begin
      if ADataSet.FindField(LColumn.ColumnName) = nil then
      begin
         TFieldSingleton.GetInstance.AddField(ADataSet,
                                              LColumn.ColumnName,
                                              LColumn.FieldType,
                                              LColumn.Size);
      end;
      /// IsWritable
      if not LColumn.PropertyRtti.IsWritable then
        ADataSet.FieldByName(LColumn.ColumnName).ReadOnly := True;
      /// IsJoinColumn
      if LColumn.IsJoinColumn then
        ADataSet.FieldByName(LColumn.ColumnName).ReadOnly := True;
      /// Required the restriction
      if LColumn.IsRequired then
        ADataSet.FieldByName(LColumn.ColumnName).Required := True;
      /// Hidden the restriction
      if LColumn.IsHidden then
        ADataSet.FieldByName(LColumn.ColumnName).Visible := False;
    end;
    /// Trata AutoInc
    LPrimaryKey := TMappingExplorer.GetInstance.GetMappingPrimaryKey(AObject.ClassType);
    if LPrimaryKey <> nil then
    begin
      if LPrimaryKey.AutoIncrement then
      begin
        for LFor := 0 to LPrimaryKey.Columns.Count -1 do
          ADataSet.FieldByName(LPrimaryKey.Columns[LFor]).DefaultExpression := '-1';
      end;
    end;
    /// <summary>
    /// TField para controle interno ao Dataset
    /// </summary>
    TFieldSingleton.GetInstance.AddField(ADataSet, cInternalField, ftInteger);
    ADataSet.Fields[ADataSet.Fields.Count -1].DefaultExpression := '-1';
    ADataSet.Fields[ADataSet.Fields.Count -1].Visible := False;
    ADataSet.Fields[ADataSet.Fields.Count -1].Index   := 0;
    /// <summary>
    /// Adiciona Fields Calcs
    /// </summary>
    SetCalcFieldDefsObjectClass(ADataSet, AObject);
    /// <summary>
    /// Adicionar Fields Aggregates
    /// </summary>
    SetAggregateFieldDefsObjectClass(ADataSet, AObject);
  finally
  end;
end;

end.
