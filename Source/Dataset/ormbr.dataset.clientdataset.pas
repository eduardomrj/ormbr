{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dataset.clientdataset;

interface

uses
  Classes,
  SysUtils,
  DB,
  Rtti,
  DBClient,
  Variants,
  Generics.Collections,
  /// orm
  ormbr.criteria,
  ormbr.dataset.adapter,
  ormbr.dataset.events,
  ormbr.factory.interfaces,
  ormbr.mapping.classes,
  ormbr.types.mapping,
  ormbr.objects.helper,
  ormbr.rtti.helper,
  ormbr.mapping.exceptions,
  ormbr.mapping.attributes;

type
  /// <summary>
  /// Captura de eventos espec�ficos do componente TClientDataSet
  /// </summary>
  TClientDataSetEvents = class(TDataSetEvents)
  private
    FBeforeApplyUpdates: TRemoteEvent;
    FAfterApplyUpdates: TRemoteEvent;
  public
    property BeforeApplyUpdates: TRemoteEvent read FBeforeApplyUpdates write FBeforeApplyUpdates;
    property AfterApplyUpdates: TRemoteEvent read FAfterApplyUpdates write FAfterApplyUpdates;
  end;

  /// <summary>
  /// Adapter TClientDataSet para controlar o Modelo e o Controle definido por:
  /// M - Object Model
  /// </summary>
  TClientDataSetAdapter<M: class, constructor> = class(TDataSetAdapter<M>)
  private
    FOrmDataSet: TClientDataSet;
    FClientDataSetEvents: TClientDataSetEvents;
    procedure DoBeforeApplyUpdates(Sender: TObject; var OwnerData: OleVariant);
    procedure DoAfterApplyUpdates(Sender: TObject; var OwnerData: OleVariant);
    procedure ExecuteCheckNotNull;
  protected
    procedure OpenInternal(ASQL: String; AID: TValue); override;
    procedure DoBeforePost(DataSet: TDataSet); override;
    procedure EmptyDataSetChilds; override;
    procedure GetDataSetEvents; override;
    procedure SetDataSetEvents; override;
    procedure ApplyInserter(MaxErros: Integer); override;
    procedure ApplyUpdater(MaxErros: Integer); override;
    procedure ApplyDeleter(MaxErros: Integer); override;
    procedure ApplyInternal(MaxErros: Integer); override;
    /// Comandos DataSet
    procedure ApplyUpdates(MaxErros: Integer); override;
  public
    constructor Create(AConnection: IDBConnection; ADataSet:
      TDataSet; APageSize: Integer; AMasterObject: TObject); override;
    destructor Destroy; override;
  end;

implementation

uses
  ormbr.dataset.bind,
  ormbr.dataset.fields;

{ TClientDataSetAdapter<M> }

constructor TClientDataSetAdapter<M>.Create(AConnection: IDBConnection;
  ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject);
begin
  inherited Create(AConnection, ADataSet, APageSize, AMasterObject);
  /// <summary>
  /// Captura o component TClientDataset da IDE passado como par�metro
  /// </summary>
  FOrmDataSet := ADataSet as TClientDataSet;
  FClientDataSetEvents := TClientDataSetEvents.Create;
  /// <summary>
  /// Captura e guarda os eventos do dataset
  /// </summary>
  GetDataSetEvents;
  /// <summary>
  /// Seta os eventos do ORM no dataset, para que ele sejam disparados
  /// </summary>
  SetDataSetEvents;
end;

destructor TClientDataSetAdapter<M>.Destroy;
begin
  FOrmDataSet := nil;
  FClientDataSetEvents.Free;
  inherited;
end;

procedure TClientDataSetAdapter<M>.DoAfterApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  if Assigned(FClientDataSetEvents.AfterApplyUpdates) then
    FClientDataSetEvents.AfterApplyUpdates(Sender, OwnerData);
end;

procedure TClientDataSetAdapter<M>.DoBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  if Assigned(FClientDataSetEvents.BeforeApplyUpdates) then
    FClientDataSetEvents.BeforeApplyUpdates(Sender, OwnerData);
end;

procedure TClientDataSetAdapter<M>.DoBeforePost(DataSet: TDataSet);
begin
  inherited DoBeforePost(DataSet);
  /// <summary>
  /// Rotina de valida��o se o campo foi deixado null
  /// </summary>
  ExecuteCheckNotNull;
end;

procedure TClientDataSetAdapter<M>.EmptyDataSetChilds;
var
  oChild: TPair<string, TDataSetAdapter<M>>;
begin
  inherited;
  for oChild in FMasterObject do
  begin
     if TClientDataSetAdapter<M>(oChild.Value).FOrmDataSet.Active then
       TClientDataSetAdapter<M>(oChild.Value).FOrmDataSet.EmptyDataSet;
  end;
end;

procedure TClientDataSetAdapter<M>.GetDataSetEvents;
begin
  inherited;
  if Assigned(FOrmDataSet.BeforeApplyUpdates) then
    FClientDataSetEvents.BeforeApplyUpdates := FOrmDataSet.BeforeApplyUpdates;
  if Assigned(FOrmDataSet.AfterApplyUpdates)  then
    FClientDataSetEvents.AfterApplyUpdates  := FOrmDataSet.AfterApplyUpdates;
end;

procedure TClientDataSetAdapter<M>.ApplyInternal(MaxErros: Integer);
var
  oRecnoBook: TBookmark;
  oProperty: TRttiProperty;
begin
  oRecnoBook := FOrmDataSet.Bookmark;
  FOrmDataSet.DisableControls;
  DisableDataSetEvents;
  try
    ApplyInserter(MaxErros);
    ApplyUpdater(MaxErros);
    ApplyDeleter(MaxErros);
  finally
    FOrmDataSet.GotoBookmark(oRecnoBook);
    FOrmDataSet.FreeBookmark(oRecnoBook);
    FOrmDataSet.EnableControls;
    EnableDataSetEvents;
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyDeleter(MaxErros: Integer);
var
  iFor: Integer;
begin
  inherited;
  /// Filtar somente os registros exclu�dos
  if FSession.DeleteList.Count > 0 then
  begin
    for iFor := 0 to FSession.DeleteList.Count -1 do
      FSession.Delete(FSession.DeleteList.Items[iFor]);
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyInserter(MaxErros: Integer);
var
  oProperty: TRttiProperty;
begin
  inherited;
  /// Filtar somente os registros inseridos
  FOrmDataSet.Filter := cInternalField + '=' + IntToStr(Integer(dsInsert));
  FOrmDataSet.Filtered := True;
  FOrmDataSet.First;
  try
    while FOrmDataSet.RecordCount > 0 do
    begin
       /// Append/Insert
       if TDataSetState(FOrmDataSet.Fields[FInternalIndex].AsInteger) in [dsInsert] then
       begin
         /// <summary>
         /// Ao passar como parametro a propriedade Current, e disparado o metodo
         /// que atualiza a var FCurrentRecordInternal, para ser usada abaixo.
         /// </summary>
         FSession.Insert(Current);
         FOrmDataSet.Edit;
         if FSession.ExistSequence then
           for oProperty in FCurrentRecordInternal.GetPrimaryKey do
             FOrmDataSet.Fields[oProperty.GetIndex].Value := oProperty.GetNullableValue(TObject(FCurrentRecordInternal)).AsVariant;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
       end;
    end;
  finally
    FOrmDataSet.Filtered := False;
    FOrmDataSet.Filter := '';
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyUpdater(MaxErros: Integer);
var
  oProperty: TRttiProperty;
begin
  inherited;
  /// Filtar somente os registros modificados
  FOrmDataSet.Filter := cInternalField + '=' + IntToStr(Integer(dsEdit));
  FOrmDataSet.Filtered := True;
  FOrmDataSet.First;
  try
    while FOrmDataSet.RecordCount > 0 do
    begin
       /// Edit
       if TDataSetState(FOrmDataSet.Fields[FInternalIndex].AsInteger) in [dsEdit] then
       begin
         if FSession.ModifiedFields.Count > 0 then
           FSession.Update(Current);
         FOrmDataSet.Edit;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
       end;
    end;
  finally
    FOrmDataSet.Filtered := False;
    FOrmDataSet.Filter := '';
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyUpdates(MaxErros: Integer);
var
  oDetail: TPair<string, TDataSetAdapter<M>>;
  oOwnerData: OleVariant;
begin
  inherited;
  if not FConnection.IsConnected then
    FConnection.Connect;
  try
    FConnection.StartTransaction;
    /// Before Apply
    DoBeforeApplyUpdates(FOrmDataSet, oOwnerData);
    try
      ApplyInternal(MaxErros);
      /// Apply Details
      for oDetail in FMasterObject do
        oDetail.Value.ApplyInternal(MaxErros);
      /// After Apply
      DoAfterApplyUpdates(FOrmDataSet, oOwnerData);
      FConnection.Commit;
    except
      FConnection.Rollback;
    end;
  finally
    FSession.ModifiedFields.Clear;
    FSession.DeleteList.Clear;
    if FConnection.IsConnected then
      FConnection.Disconnect;
  end;
end;

procedure TClientDataSetAdapter<M>.OpenInternal(ASQL: String; AID: TValue);
begin
  FOrmDataSet.DisableControls;
  DisableDataSetEvents;
  if not FConnection.IsConnected then
    FConnection.Connect;
  try
    if not FOrmDataSet.Active then
    begin
       FOrmDataSet.CreateDataSet;
       FOrmDataSet.LogChanges := False;
    end;
    FOrmDataSet.EmptyDataSet;
    inherited OpenInternal(ASQL, AID);
  finally
    EnableDataSetEvents;
    FOrmDataSet.First;
    FOrmDataSet.EnableControls;
    if FConnection.IsConnected then
      FConnection.Disconnect;
  end;
end;

procedure TClientDataSetAdapter<M>.SetDataSetEvents;
begin
  inherited;
  FOrmDataSet.BeforeApplyUpdates := DoBeforeApplyUpdates;
  FOrmDataSet.AfterApplyUpdates  := DoAfterApplyUpdates;
end;

/// <summary>
/// Essa rotina requer na clausula USES a unit ormbr.object.helper, no D2010
/// ao adicionar a unit na clausula USES da classe m�e deu um error internal
/// por isso essa rotina foi descrita nas duas subclass, onde o error n�o ocorreu.
/// </summary>
procedure TClientDataSetAdapter<M>.ExecuteCheckNotNull;
var
  oColumn: TColumnMapping;
  oColumns: TColumnMappingList;
begin
  oColumns := FSession.Explorer.GetMappingColumn(FCurrentRecordInternal.ClassType);
  for oColumn in oColumns do
  begin
    if oColumn.IsJoinColumn then
      Continue;
    if oColumn.IsNotNull then
      if FOrmDataSet.Fields[oColumn.FieldIndex +1].Value = Null then
        raise EFieldNotNull.Create(TObject(FCurrentRecordInternal).GetTable.Name + '.' + oColumn.ColumnName);
  end;
end;

end.
