{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dataset.fdmemtable;

interface

uses
  Classes,
  SysUtils,
  DB,
  Rtti,
  Variants,
  Generics.Collections,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Stan.Async,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  /// orm
  ormbr.criteria,
  ormbr.dataset.adapter,
  ormbr.dataset.events,
  ormbr.factory.interfaces,
  ormbr.types.mapping,
  ormbr.mapping.classes,
  ormbr.rtti.helper,
  ormbr.objects.helper,
  ormbr.mapping.exceptions,
  ormbr.mapping.attributes;

type
  /// <summary>
  /// Captura de eventos espec�ficos do componente TFDMemTable
  /// </summary>
  TFDMemTableEvents = class(TDataSetEvents)
  private
    FBeforeApplyUpdates: TFDDataSetEvent;
    FAfterApplyUpdates: TFDAfterApplyUpdatesEvent;
  public
    property BeforeApplyUpdates: TFDDataSetEvent read FBeforeApplyUpdates write FBeforeApplyUpdates;
    property AfterApplyUpdates: TFDAfterApplyUpdatesEvent read FAfterApplyUpdates write FAfterApplyUpdates;
  end;

  /// <summary>
  /// Adapter TClientDataSet para controlar o Modelo e o Controle definido por:
  /// M - Object Model
  /// </summary>
  TFDMemTableAdapter<M: class, constructor> = class(TDataSetAdapter<M>)
  private
    FOrmDataSet: TFDMemTable;
    FMemTableEvents: TFDMemTableEvents;
    procedure DoBeforeApplyUpdates(DataSet: TFDDataSet);
    procedure DoAfterApplyUpdates(DataSet: TFDDataSet; AErrors: Integer);
    procedure ExecuteCheckNotNull;
    procedure SetAutoIncValueChilds(AField: TField);
  protected
    procedure OpenInternal(ASQL: String; AID: TValue); override;
    procedure DoBeforePost(DataSet: TDataSet); override;
    procedure EmptyDataSetChilds; override;
    procedure GetDataSetEvents; override;
    procedure SetDataSetEvents; override;
    procedure ApplyInserter(MaxErros: Integer); override;
    procedure ApplyUpdater(MaxErros: Integer); override;
    procedure ApplyDeleter(MaxErros: Integer); override;
    procedure ApplyInternal(MaxErros: Integer); override;
    /// Comandos DataSet
    procedure ApplyUpdates(MaxErros: Integer); override;
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet;
      APageSize: Integer; AMasterObject: TObject); override;
    destructor Destroy; override;
  end;

implementation

uses
  ormbr.dataset.bind,
  ormbr.dataset.fields;

{ TFDMemTableAdapter<M> }

constructor TFDMemTableAdapter<M>.Create(AConnection: IDBConnection; ADataSet: TDataSet;
  APageSize: Integer; AMasterObject: TObject);
begin
  inherited Create(ACOnnection, ADataSet, APageSize, AMasterObject);
  /// <summary>
  /// Captura o component TFDMemTable da IDE passado como par�metro
  /// </summary>
  FOrmDataSet := ADataSet as TFDMemTable;
  FMemTableEvents := TFDMemTableEvents.Create;
  /// <summary>
  /// Captura e guarda os eventos do dataset
  /// </summary>
  GetDataSetEvents;
  /// <summary>
  /// Seta os eventos do ORM no dataset, para que ele sejam disparados
  /// </summary>
  SetDataSetEvents;
end;

destructor TFDMemTableAdapter<M>.Destroy;
begin
  FOrmDataSet := nil;
  FMemTableEvents.Free;
  inherited;
end;

procedure TFDMemTableAdapter<M>.DoAfterApplyUpdates(DataSet: TFDDataSet; AErrors: Integer);
begin
  if Assigned(FMemTableEvents.AfterApplyUpdates) then
     FMemTableEvents.AfterApplyUpdates(DataSet, AErrors);
end;

procedure TFDMemTableAdapter<M>.DoBeforeApplyUpdates(DataSet: TFDDataSet);
begin
  if Assigned(FMemTableEvents.BeforeApplyUpdates) then
     FMemTableEvents.BeforeApplyUpdates(DataSet);
end;

procedure TFDMemTableAdapter<M>.DoBeforePost(DataSet: TDataSet);
begin
  inherited DoBeforePost(DataSet);
  /// <summary>
  /// Rotina de valida��o se o campo foi deixado null
  /// </summary>
  ExecuteCheckNotNull;
end;

procedure TFDMemTableAdapter<M>.EmptyDataSetChilds;
var
  oChild: TPair<string, TDataSetAdapter<M>>;
begin
  inherited;
  for oChild in FMasterObject do
  begin
     if TFDMemTableAdapter<M>(oChild.Value).FOrmDataSet.Active then
       TFDMemTableAdapter<M>(oChild.Value).FOrmDataSet.EmptyDataSet;
  end;
end;

procedure TFDMemTableAdapter<M>.GetDataSetEvents;
begin
  inherited;
  if Assigned(FOrmDataSet.BeforeApplyUpdates) then
    FMemTableEvents.BeforeApplyUpdates := FOrmDataSet.BeforeApplyUpdates;
  if Assigned(FOrmDataSet.AfterApplyUpdates)  then
    FMemTableEvents.AfterApplyUpdates  := FOrmDataSet.AfterApplyUpdates;
end;

procedure TFDMemTableAdapter<M>.ApplyInternal(MaxErros: Integer);
var
  oRecnoBook: TBookmark;
begin
  oRecnoBook := FOrmDataSet.GetBookmark;
  FOrmDataSet.DisableControls;
  FOrmDataSet.DisableConstraints;
  DisableDataSetEvents;
  try
    ApplyInserter(MaxErros);
    ApplyUpdater(MaxErros);
    ApplyDeleter(MaxErros);
  finally
    FOrmDataSet.GotoBookmark(oRecnoBook);
    FOrmDataSet.FreeBookmark(oRecnoBook);
    FOrmDataSet.EnableConstraints;
    FOrmDataSet.EnableControls;
    EnableDataSetEvents;
  end;
end;

procedure TFDMemTableAdapter<M>.ApplyDeleter(MaxErros: Integer);
var
  iFor: Integer;
begin
  inherited;
  /// Filtar somente os registros exclu�dos
  if FSession.DeleteList.Count > 0 then
  begin
    for iFor := 0 to FSession.DeleteList.Count -1 do
      FSession.Delete(FSession.DeleteList.Items[iFor]);
  end;
end;

procedure TFDMemTableAdapter<M>.ApplyInserter(MaxErros: Integer);
var
  oProperty: TRttiProperty;
begin
  inherited;
  /// Filtar somente os registros inseridos
  FOrmDataSet.Filter := cInternalField + '=' + IntToStr(Integer(dsInsert));
  FOrmDataSet.Filtered := True;
  FOrmDataSet.First;
  try
    while FOrmDataSet.RecordCount > 0 do
    begin
       /// Append/Insert
       if TDataSetState(FOrmDataSet.Fields[FInternalIndex].AsInteger) in [dsInsert] then
       begin
         /// <summary>
         /// Ao passar como parametro a propriedade Current, e disparado o metodo
         /// que atualiza a var FCurrentRecordInternal, para ser usada abaixo.
         /// </summary>
         FSession.Insert(Current);
         FOrmDataSet.Edit;
         if FSession.ExistSequence then
         begin
           for oProperty in FCurrentRecordInternal.GetPrimaryKey do
           begin
             FOrmDataSet.Fields[oProperty.GetIndex].Value := oProperty.GetValue(TObject(FCurrentRecordInternal)).AsVariant;
             /// <summary>
             /// Atualiza o valor do AutoInc nas sub tabelas
             /// </summary>
             SetAutoIncValueChilds(FOrmDataSet.Fields[oProperty.GetIndex]);
           end;
         end;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
       end;
    end;
  finally
    FOrmDataSet.Filtered := False;
    FOrmDataSet.Filter := '';
  end;
end;

procedure TFDMemTableAdapter<M>.SetAutoIncValueChilds(AField: TField);
var
  oAssociations: TAssociationMappingList;
  oAssociation: TAssociationMapping;
  oDataSetChild: TDataSetAdapter<M>;
begin
  oAssociations := FSession.Explorer.GetMappingAssociation(FCurrentRecordInternal.ClassType);
  if oAssociations <> nil then
  begin
    for oAssociation in oAssociations do
    begin
      if oAssociation.Multiplicity in [OneToMany] then
      begin
        oDataSetChild := FMasterObject.Items[oAssociation.ClassNameRef];
        if oDataSetChild <> nil then
        begin
          if oDataSetChild.FOrmDataSet.FindField(oAssociation.ColumnsNameRef[0]) <> nil then
          begin
            oDataSetChild.FOrmDataSet.DisableControls;
            oDataSetChild.FOrmDataSet.First;
            try
              while not oDataSetChild.FOrmDataSet.Eof do
              begin
                oDataSetChild.FOrmDataSet.Edit;
                oDataSetChild.FOrmDataSet.FieldByName(oAssociation.ColumnsNameRef[0]).Value := AField.Value;
                oDataSetChild.FOrmDataSet.Post;
                oDataSetChild.FOrmDataSet.Next;
              end;
            finally
              oDataSetChild.FOrmDataSet.First;
              oDataSetChild.FOrmDataSet.EnableControls;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFDMemTableAdapter<M>.ApplyUpdater(MaxErros: Integer);
var
  oProperty: TRttiProperty;
begin
  inherited;
  /// Filtar somente os registros modificados
  FOrmDataSet.Filter := cInternalField + '=' + IntToStr(Integer(dsEdit));
  FOrmDataSet.Filtered := True;
  FOrmDataSet.First;
  try
    while FOrmDataSet.RecordCount > 0 do
    begin
       /// Edit
       if TDataSetState(FOrmDataSet.Fields[FInternalIndex].AsInteger) in [dsEdit] then
       begin
         if FSession.ModifiedFields.Count > 0 then
           FSession.Update(Current);
         FOrmDataSet.Edit;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
       end;
    end;
  finally
    FOrmDataSet.Filtered := False;
    FOrmDataSet.Filter := '';
  end;
end;

procedure TFDMemTableAdapter<M>.ApplyUpdates(MaxErros: Integer);
var
  oDetail: TPair<string, TDataSetAdapter<M>>;
begin
  inherited;
  if not FConnection.IsConnected then
     FConnection.Connect;
  try
    FConnection.StartTransaction;
    /// Before Apply
    DoBeforeApplyUpdates(FOrmDataSet);
    try
      ApplyInternal(MaxErros);
      /// Apply Details
      for oDetail in FMasterObject do
        oDetail.Value.ApplyInternal(MaxErros);
      /// After Apply
      DoAfterApplyUpdates(FOrmDataSet, MaxErros);
      FConnection.Commit;
    except
      FConnection.Rollback;
    end;
  finally
    FSession.ModifiedFields.Clear;
    FSession.DeleteList.Clear;
    if FConnection.IsConnected then
      FConnection.Disconnect;
  end;
end;

procedure TFDMemTableAdapter<M>.OpenInternal(ASQL: String; AID: TValue);
begin
  FOrmDataSet.DisableControls;
  FOrmDataSet.DisableConstraints;
  DisableDataSetEvents;
  FConnection.Connect;
  try
    if not FOrmDataSet.Active then
    begin
      FOrmDataSet.CreateDataSet;
      FOrmDataSet.CachedUpdates := True;
      FOrmDataSet.LogChanges := False;
      FOrmDataSet.FetchOptions.RecsMax := 300000;
      FOrmDataSet.ResourceOptions.SilentMode := True;
      FOrmDataSet.UpdateOptions.LockMode := lmNone;
      FOrmDataSet.UpdateOptions.LockPoint := lpDeferred;
      FOrmDataSet.UpdateOptions.FetchGeneratorsPoint := gpImmediate;
      FOrmDataSet.Open;
    end;
    FOrmDataSet.EmptyDataSet;
    inherited OpenInternal(ASQL, AID);
  finally
    EnableDataSetEvents;
    FOrmDataSet.First;
    FOrmDataSet.EnableConstraints;
    FOrmDataSet.EnableControls;
    FConnection.Disconnect;
  end;
end;

procedure TFDMemTableAdapter<M>.SetDataSetEvents;
begin
  inherited;
  FOrmDataSet.BeforeApplyUpdates := DoBeforeApplyUpdates;
  FOrmDataSet.AfterApplyUpdates  := DoAfterApplyUpdates;
end;

/// <summary>
/// Essa rotina requer na clausula USES a unit ormbr.mapping.helper, no D2010
/// ao adicionar a unit na clausula USES da classe m�e deu um error internal
//  por isso essa rotina foi descrita nas duas subclass, a onde � error n�o ocorreu.
/// </summary>
procedure TFDMemTableAdapter<M>.ExecuteCheckNotNull;
var
  oColumn: TColumnMapping;
  oColumns: TColumnMappingList;
begin
  oColumns := FSession.Explorer.GetMappingColumn(FCurrentRecordInternal.ClassType);
  for oColumn in oColumns do
  begin
    if oColumn.IsJoinColumn then
      Continue;
    if oColumn.IsNotNull then
      if FOrmDataSet.Fields[oColumn.FieldIndex +1].Value = Null then
        raise EFieldNotNull.Create(TObject(FCurrentRecordInternal).GetTable.Name + '.' + oColumn.ColumnName);
  end;
end;

end.
