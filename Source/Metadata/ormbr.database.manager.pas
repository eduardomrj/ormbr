{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.database.manager;

interface

uses
  SysUtils,
  ormbr.factory.interfaces,
  ormbr.metadata.db.factory,
  ormbr.metadata.classe.factory,
  ormbr.database.factory,
  ormbr.types.database;

type
  TDatabaseManager = class(TDatabaseFactory)
  protected
    FConnTarget: IDBConnection;
    FMetadataMaster: TMetadataClasseAbstract;
    FMetadataTarget: TMetadataDBAbstract;
    procedure ExtractDatabase; override;
    procedure ExecuteDDLCommands; override;
  public
    constructor Create(AConnTarget: IDBConnection); overload;
    destructor Destroy; override;
  end;

implementation

uses
  ormbr.ddl.commands;

{ TDatabaseManager }

constructor TDatabaseManager.Create(AConnTarget: IDBConnection);
begin
  FConnTarget := AConnTarget;
  FConnTarget.Connect;
  if not FConnTarget.IsConnected then
    raise Exception.Create('N�o foi possivel fazer conex�o com o banco de dados Target');

  inherited Create(AConnTarget.GetDriverName);
  FMetadataMaster := TMetadataClasseFactory.Create(FConnTarget, FCatalogMaster);
  FMetadataTarget := TMetadataDBFactory.Create(FConnTarget, FCatalogTarget);
end;

destructor TDatabaseManager.Destroy;
begin
  FMetadataTarget.Free;
  FMetadataMaster.Free;
  inherited;
end;

procedure TDatabaseManager.ExecuteDDLCommands;
var
  oCommand: TDDLCommand;
  sCommand: string;
begin
  inherited;
  FConnTarget.StartTransaction;
  try
    for oCommand in FDDLCommands do
    begin
      sCommand := oCommand.BuildCommand(FGeneratorCommand);
      if Length(sCommand) > 0 then
        if FCommandsAutoExecute then
          FConnTarget.ExecuteScript(sCommand);
    end;
    FConnTarget.Commit;
  except
    on E: Exception do
    begin
      FConnTarget.Rollback;
      raise Exception.Create('ORMBr Command : [' + oCommand.Warning + '] - ' + E.Message + sLineBreak +
                             'Script : "' + sCommand + '"');
    end;
  end;
end;

procedure TDatabaseManager.ExtractDatabase;
begin
  inherited;
  /// <summary>
  /// Extrai todo metadata com base nos modelos existentes
  /// </summary>
  FMetadataMaster.ExtractMetadata;
  /// <summary>
  /// Extrai todo metadata com base banco de dados acessado
  /// </summary>
  FMetadataTarget.ExtractMetadata;
end;

end.
